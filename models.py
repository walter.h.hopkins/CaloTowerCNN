#!/usr/bin/env python
from keras.models import Sequential, load_model, Model
from keras.layers.core import Dense, Activation, Dropout, Flatten
from keras.layers.normalization import BatchNormalization
from keras.layers import Conv2D, MaxPooling2D, Input
from keras.layers.merge import Concatenate
from keras.layers.advanced_activations import LeakyReLU, PReLU
from keras import initializers

def modelConv2D_5x5(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(32, (5,5), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (5,5), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (5,5), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_7x7(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(32, (7,7), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (7,7), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (7,7), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_5x5_16Filters(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (5,5), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (5,5), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (5,5), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_5x5_8Filters(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(8, (5,5), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(8, (5,5), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(8, (5,5), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_5x5_8Filters_stride4x4(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(8, (5,5), activation='relu', input_shape=inputShape, padding='same', strides=(4,4)))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(8, (5,5), activation='relu', padding='same', strides=(4,4)))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(8, (5,5), activation='relu', padding='same', strides=(4,4)))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_5x5_8Filters_2layers(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(8, (5,5), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(8, (5,5), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_5x5_8Filters_1layers(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(8, (5,5), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_7x7_16Filters(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (7,7), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (7,7), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (7,7), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_7x7_8Filters(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(8, (7,7), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(8, (7,7), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(8, (7,7), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_7x7_8Filters_stride4x4(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(8, (7,7), activation='relu', input_shape=inputShape, strides=(4,4), padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(8, (7,7), activation='relu', padding='same', strides=(4,4)))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(8, (7,7), activation='relu', padding='same', strides=(4,4)))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_1(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(32, (3, 3), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3,3), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3,3), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(64, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_1_noMaxPool(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(32, (3, 3), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3,3), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3,3), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Conv2D(64, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_2(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(32, (3, 3), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Conv2D(32, (3,3), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Conv2D(64, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())

    model.add(Conv2D(32, (3,3), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Conv2D(32, (3,3), activation='relu', padding='same'))
    model.add(BatchNormalization())
    
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_2_noMaxPool(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(32, (3, 3), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Conv2D(32, (3,3), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Conv2D(64, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())

    model.add(Conv2D(32, (3,3), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Conv2D(32, (3,3), activation='relu', padding='same'))
    model.add(BatchNormalization())
    
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_2_prelu(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(32, (3, 3), input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(PReLU())

    model.add(Conv2D(32, (3,3), padding='same'))
    model.add(BatchNormalization())
    model.add(PReLU())
    model.add(Dropout(0.3))
    
    model.add(Conv2D(64, (3,3), padding='same'))
    model.add(BatchNormalization())
    model.add(PReLU())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same'))
    model.add(BatchNormalization())
    model.add(PReLU())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same'))
    model.add(BatchNormalization())
    model.add(PReLU())

    model.add(Conv2D(32, (3,3), padding='same'))
    model.add(BatchNormalization())
    model.add(PReLU())
    model.add(Conv2D(32, (3,3), padding='same'))
    model.add(BatchNormalization())
    model.add(PReLU())
    
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(64, (3, 3), padding='same'))
    model.add(BatchNormalization())
    model.add(PReLU())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same'))
    model.add(BatchNormalization())
    model.add(PReLU())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32))
    model.add(BatchNormalization())
    model.add(PReLU())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_2_prelu_noMaxPool(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(32, (3, 3), input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(PReLU())

    model.add(Conv2D(32, (3,3), padding='same'))
    model.add(BatchNormalization())
    model.add(PReLU())
    model.add(Dropout(0.3))
    
    model.add(Conv2D(64, (3,3), padding='same'))
    model.add(BatchNormalization())
    model.add(PReLU())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same'))
    model.add(BatchNormalization())
    model.add(PReLU())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same'))
    model.add(BatchNormalization())
    model.add(PReLU())

    model.add(Conv2D(32, (3,3), padding='same'))
    model.add(BatchNormalization())
    model.add(PReLU())
    model.add(Conv2D(32, (3,3), padding='same'))
    model.add(BatchNormalization())
    model.add(PReLU())
    
    model.add(Conv2D(64, (3, 3), padding='same'))
    model.add(BatchNormalization())
    model.add(PReLU())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same'))
    model.add(BatchNormalization())
    model.add(PReLU())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32))
    model.add(BatchNormalization())
    model.add(PReLU())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_1_deeper(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(64, (3, 3), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3,3), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3,3), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(128, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))  
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(512, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_1_deeper_noMaxPool(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(64, (3, 3), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3,3), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3,3), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Conv2D(128, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(Conv2D(512, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))  
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(512, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_1_lessFilters(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (3, 3), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_1_lessFilters_noMaxPool(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (3, 3), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_1_lessFilters_deeper(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (3, 3), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_1_lessFilters_deeper_noMaxPool(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (3, 3), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_1_lessFilters_deeper_lessMaxPool(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (3, 3), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_1_lessFilters_5x5(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (5, 5), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (5, 5), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (5, 5), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(32, (5, 5), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (5, 5), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (5, 5), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_simple(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(16, (3, 3), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3,3), activation='relu'))
    model.add(Dropout(0.3))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_1Conv(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(3, (3, 3), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dropout(0.3))

    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_1Conv_nopadding(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(3, (3, 3), activation='relu', input_shape=inputShape))#, padding='same'))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dropout(0.3))

    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_1Conv_nopadding_1filters_3x3(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(1, (3, 3), activation='relu', input_shape=inputShape))#, padding='same'))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dropout(0.3))

    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_1Conv_nopadding_2filters_3x3(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(2, (3, 3), activation='relu', input_shape=inputShape))#, padding='same'))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dropout(0.3))

    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_1Conv_nopadding_2filters_3x3_8fc(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(2, (3, 3), activation='relu', input_shape=inputShape))#, padding='same'))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dropout(0.3))

    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(8, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_1Conv_nopadding_3filters_3x3(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(3, (3, 3), activation='relu', input_shape=inputShape))#, padding='same'))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dropout(0.3))

    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_1Conv_nopadding_4filters_3x3(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(4, (3, 3), activation='relu', input_shape=inputShape))#, padding='same'))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dropout(0.3))

    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_1Conv_nopadding_1filters_7x7(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(1, (7, 7), activation='relu', input_shape=inputShape))#, padding='same'))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dropout(0.3))

    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_1Conv_nopadding_1filters_9x9(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(1, (9, 9), activation='relu', input_shape=inputShape))#, padding='same'))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dropout(0.3))

    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_1Conv_nopadding_5filters_7x7(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(5, (7, 7), activation='relu', input_shape=inputShape))#, padding='same'))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dropout(0.3))

    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_1Conv_nopadding_5filters_11x11(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(5, (11, 11), activation='relu', input_shape=inputShape))#, padding='same'))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dropout(0.3))

    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model



def modelConv2D_1Conv_nopadding_9filters_9x9(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(9, (9, 9), activation='relu', input_shape=inputShape))#, padding='same'))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dropout(0.3))

    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_1Conv_nopadding_9filters_9x9_stride5x5(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(9, (9, 9), activation='relu', input_shape=inputShape, strides=(5,5)))#, padding='same'))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dropout(0.3))

    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_1Conv_nopadding_20filters_9x9(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(20, (9, 9), activation='relu', input_shape=inputShape))#, padding='same'))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dropout(0.3))

    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_1Conv_nopadding_4filters_9x9(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(4, (9, 9), activation='relu', input_shape=inputShape))#, padding='same'))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dropout(0.3))

    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_1Conv_nopadding_4filters_9x9_stride2x2(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(4, (9, 9), activation='relu', input_shape=inputShape, strides=(2,2)))#, padding='same'))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dropout(0.3))

    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_1Conv_nopadding_11filters_11x11(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(11, (11, 11), activation='relu', input_shape=inputShape))#, padding='same'))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dropout(0.3))

    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_1Conv_nopadding_stride2x2(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(3, (3, 3), activation='relu', input_shape=inputShape, strides=(2,2)))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dropout(0.3))

    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_1Conv_stride2(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(3, (3, 3), activation='relu', input_shape=inputShape, padding='same', strides=(2,2)))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Flatten())
    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_1Conv_5x5(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(5, (5, 5), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Flatten())
    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model



def modelConv2D_1Conv_5x5_stride2(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(3, (5, 5), activation='relu', input_shape=inputShape, padding='same', strides=(2,2)))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Flatten())
    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_1Conv_7x7(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(7, (7, 7), activation='relu', input_shape=inputShape, padding='same'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Flatten())
    #model.add(Dense(16, activation='relu'))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file 
    model.save(modelFPath+modelName+'.h5')
    return model



def modelConv2D_simple_prelu(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Conv2D(16, (3, 3), input_shape=inputShape, padding='same'))
    model.add(PReLU())
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3,3), padding='same'))
    model.add(PReLU())
    model.add(Dropout(0.3))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
     
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_inception(inputShape, modelName, modelFPath):
    """Different filter sizes on the same input"""
    input_img = Input(shape=inputShape)
    tower_1 = Conv2D(16, (1, 1), padding='same', activation='relu')(input_img)
    tower_1 = Conv2D(16, (3, 3), padding='same', activation='relu')(tower_1)

    tower_2 = Conv2D(16, (1, 1), padding='same', activation='relu')(input_img)
    tower_2 = Conv2D(16, (5, 5), padding='same', activation='relu')(tower_2)

    tower_3 = Conv2D(16, (1, 1), padding='same', activation='relu')(input_img)
    tower_3 = Conv2D(16, (7, 7), padding='same', activation='relu')(tower_3)

    out = Concatenate(axis=1)([tower_1, tower_2, tower_3])
    model = Sequential()
    model.add(Model(inputs=[input_img], outputs=out))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))


    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_inception_upTo11(inputShape, modelName, modelFPath):
    """Different filter sizes on the same input"""
    input_img = Input(shape=inputShape)
    tower_1 = Conv2D(16, (1, 1), padding='same', activation='relu')(input_img)
    tower_1 = Conv2D(16, (3, 3), padding='same', activation='relu')(tower_1)

    tower_2 = Conv2D(16, (1, 1), padding='same', activation='relu')(input_img)
    tower_2 = Conv2D(16, (5, 5), padding='same', activation='relu')(tower_2)

    tower_3 = Conv2D(16, (1, 1), padding='same', activation='relu')(input_img)
    tower_3 = Conv2D(16, (7, 7), padding='same', activation='relu')(tower_3)

    tower_4 = Conv2D(16, (1, 1), padding='same', activation='relu')(input_img)
    tower_4 = Conv2D(16, (9, 9), padding='same', activation='relu')(tower_4)

    tower_5 = Conv2D(16, (1, 1), padding='same', activation='relu')(input_img)
    tower_5 = Conv2D(16, (11,11), padding='same', activation='relu')(tower_5)

    out = Concatenate(axis=1)([tower_1, tower_2, tower_3, tower_4, tower_5])
    model = Sequential()
    model.add(Model(inputs=[input_img], outputs=out))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))


    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_inception_upTo11_no1x1(inputShape, modelName, modelFPath):
    """Different filter sizes on the same input"""
    input_img = Input(shape=inputShape)
    tower_1 = Conv2D(16, (3, 3), padding='same', activation='relu')(input_img)

    tower_2 = Conv2D(16, (5, 5), padding='same', activation='relu')(input_img)

    tower_3 = Conv2D(16, (7, 7), padding='same', activation='relu')(input_img)

    tower_4 = Conv2D(16, (9, 9), padding='same', activation='relu')(input_img)

    tower_5 = Conv2D(16, (11, 11), padding='same', activation='relu')(input_img)

    out = Concatenate(axis=1)([tower_1, tower_2, tower_3, tower_4, tower_5])
    model = Sequential()
    model.add(Model(inputs=[input_img], outputs=out))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))


    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_inception_no1x1(inputShape, modelName, modelFPath):
    """Different filter sizes on the same input"""
    input_img = Input(shape=inputShape)
    tower_1 = Conv2D(16, (3, 3), padding='same', activation='relu')(input_img)

    tower_2 = Conv2D(16, (5, 5), padding='same', activation='relu')(input_img)

    tower_3 = Conv2D(16, (7, 7), padding='same', activation='relu')(input_img)

    out = Concatenate(axis=1)([tower_1, tower_2, tower_3])
    model = Sequential()
    model.add(Model(inputs=[input_img], outputs=out))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))


    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_inception_no1x1_5_7_11_19(inputShape, modelName, modelFPath):
    """Different filter sizes on the same input"""
    input_img = Input(shape=inputShape)
    tower_1 = Conv2D(16, (5, 5), padding='same', activation='relu')(input_img)

    tower_2 = Conv2D(16, (7, 7), padding='same', activation='relu')(input_img)

    tower_3 = Conv2D(16, (11, 11), padding='same', activation='relu')(input_img)

    tower_4 = Conv2D(16, (19, 19), padding='same', activation='relu')(input_img)

    out = Concatenate(axis=1)([tower_1, tower_2, tower_3, tower_4])
    model = Sequential()
    model.add(Model(inputs=[input_img], outputs=out))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))


    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_inception_no1x1_5_7_11(inputShape, modelName, modelFPath):
    """Different filter sizes on the same input"""
    input_img = Input(shape=inputShape)
    tower_1 = Conv2D(16, (5, 5), padding='same', activation='relu')(input_img)

    tower_2 = Conv2D(16, (7, 7), padding='same', activation='relu')(input_img)

    tower_3 = Conv2D(16, (11, 11), padding='same', activation='relu')(input_img)

    out = Concatenate(axis=1)([tower_1, tower_2, tower_3])
    model = Sequential()
    model.add(Model(inputs=[input_img], outputs=out))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))


    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_inception_noMaxPool(inputShape, modelName, modelFPath):
    """Different filter sizes on the same input"""
    input_img = Input(shape=inputShape)
    tower_1 = Conv2D(16, (1, 1), padding='same', activation='relu')(input_img)
    tower_1 = Conv2D(16, (3, 3), padding='same', activation='relu')(tower_1)

    tower_2 = Conv2D(16, (1, 1), padding='same', activation='relu')(input_img)
    tower_2 = Conv2D(16, (5, 5), padding='same', activation='relu')(tower_2)

    tower_3 = Conv2D(16, (1, 1), padding='same', activation='relu')(input_img)
    tower_3 = Conv2D(16, (7, 7), padding='same', activation='relu')(tower_3)

    out = Concatenate(axis=1)([tower_1, tower_2, tower_3])
    model = Sequential()
    model.add(Model(inputs=[input_img], outputs=out))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))


    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(16, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_inception_32Filters(inputShape, modelName, modelFPath):
    """Different filter sizes on the same input"""
    input_img = Input(shape=inputShape)
    tower_1 = Conv2D(32, (1, 1), padding='same', activation='relu')(input_img)
    tower_1 = Conv2D(32, (3, 3), padding='same', activation='relu')(tower_1)

    tower_2 = Conv2D(32, (1, 1), padding='same', activation='relu')(input_img)
    tower_2 = Conv2D(32, (5, 5), padding='same', activation='relu')(tower_2)

    tower_3 = Conv2D(32, (1, 1), padding='same', activation='relu')(input_img)
    tower_3 = Conv2D(32, (7, 7), padding='same', activation='relu')(tower_3)

    out = Concatenate(axis=1)([tower_1, tower_2, tower_3])
    model = Sequential()
    model.add(Model(inputs=[input_img], outputs=out))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_inception_32Filters_noMaxPool(inputShape, modelName, modelFPath):
    """Different filter sizes on the same input"""
    input_img = Input(shape=inputShape)
    tower_1 = Conv2D(32, (1, 1), padding='same', activation='relu')(input_img)
    tower_1 = Conv2D(32, (3, 3), padding='same', activation='relu')(tower_1)

    tower_2 = Conv2D(32, (1, 1), padding='same', activation='relu')(input_img)
    tower_2 = Conv2D(32, (5, 5), padding='same', activation='relu')(tower_2)

    tower_3 = Conv2D(32, (1, 1), padding='same', activation='relu')(input_img)
    tower_3 = Conv2D(32, (7, 7), padding='same', activation='relu')(tower_3)

    out = Concatenate(axis=1)([tower_1, tower_2, tower_3])
    model = Sequential()
    model.add(Model(inputs=[input_img], outputs=out))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_inception_32Filters_deeper(inputShape, modelName, modelFPath):
    """Different filter sizes on the same input"""
    input_img = Input(shape=inputShape)
    tower_1 = Conv2D(32, (1, 1), padding='same', activation='relu')(input_img)
    tower_1 = Conv2D(32, (3, 3), padding='same', activation='relu')(tower_1)

    tower_2 = Conv2D(32, (1, 1), padding='same', activation='relu')(input_img)
    tower_2 = Conv2D(32, (5, 5), padding='same', activation='relu')(tower_2)

    tower_3 = Conv2D(32, (1, 1), padding='same', activation='relu')(input_img)
    tower_3 = Conv2D(32, (7, 7), padding='same', activation='relu')(tower_3)

    out = Concatenate(axis=1)([tower_1, tower_2, tower_3])
    model = Sequential()
    model.add(Model(inputs=[input_img], outputs=out))
    model.add(BatchNormalization())
    #model.add(Dropout(0.4))

    tower_1 = Conv2D(32, (1, 1), padding='same', activation='relu')(input_img)
    tower_1 = Conv2D(32, (3, 3), padding='same', activation='relu')(tower_1)

    tower_2 = Conv2D(32, (1, 1), padding='same', activation='relu')(input_img)
    tower_2 = Conv2D(32, (5, 5), padding='same', activation='relu')(tower_2)

    tower_3 = Conv2D(32, (1, 1), padding='same', activation='relu')(input_img)
    tower_3 = Conv2D(32, (7, 7), padding='same', activation='relu')(tower_3)

    newOut = Concatenate(axis=1)([tower_1, tower_2, tower_3])

    model.add(Model(inputs=[input_img], outputs=newOut))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))    
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))

    model.add(MaxPooling2D(pool_size=(2,2))) 
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))   
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Flatten())    
    model.add(Dense(128, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelMLP_1(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Dense(64, input_dim=inputShape, activation='relu'))#, kernel_initializer=initializer))
    #model.add(Dropout(0.4))
    model.add(BatchNormalization())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(Dense(1, activation='sigmoid'))
    
    # Set loss and optimizer
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy',])
    
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelMLP_2(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Dense(64, input_dim=inputShape, activation='relu'))#, kernel_initializer=initializer))
    #model.add(Dropout(0.4))
    model.add(BatchNormalization())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(Dense(1, activation='sigmoid'))
    
    # Set loss and optimizer
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy',])
    
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelMLP_3(inputShape, modelName, modelFPath):
    model = Sequential()
    model.add(Dense(64, input_dim=inputShape, activation='relu'))#, kernel_initializer=initializer))
    #model.add(Dropout(0.4))
    model.add(BatchNormalization())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())


    model.add(Dense(128, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dense(128, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dense(128, activation='relu'))
    model.add(BatchNormalization())

    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(Dense(1, activation='sigmoid'))
    
    # Set loss and optimizer
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy',])
    
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

    
models = {
    "modelConv2D_5x5":modelConv2D_5x5,
    "modelConv2D_7x7":modelConv2D_7x7,
    "modelConv2D_5x5_16Filters":modelConv2D_5x5_16Filters,
    "modelConv2D_7x7_16Filters":modelConv2D_7x7_16Filters,

    "modelConv2D_5x5_8Filters":modelConv2D_5x5_8Filters,
    "modelConv2D_5x5_8Filters_stride4x4":modelConv2D_5x5_8Filters_stride4x4,
    "modelConv2D_7x7_8Filters":modelConv2D_7x7_8Filters,
    "modelConv2D_7x7_8Filters_stride4x4":modelConv2D_7x7_8Filters_stride4x4,

    "modelConv2D_5x5_8Filters_2layers":modelConv2D_5x5_8Filters_2layers,
    "modelConv2D_5x5_8Filters_1layers":modelConv2D_5x5_8Filters_1layers,

    "modelConv2D_1":modelConv2D_1,
    "modelConv2D_2":modelConv2D_2,
    "modelConv2D_2_prelu":modelConv2D_2_prelu,
    "modelConv2D_1_noMaxPool":modelConv2D_1_noMaxPool,
    "modelConv2D_2_noMaxPool":modelConv2D_2_noMaxPool,
    "modelConv2D_2_prelu_noMaxPool":modelConv2D_2_prelu_noMaxPool,
    "modelConv2D_1_lessFilters":modelConv2D_1_lessFilters,
    "modelConv2D_1_lessFilters_5x5":modelConv2D_1_lessFilters_5x5,
    "modelConv2D_1_lessFilters_deeper":modelConv2D_1_lessFilters_deeper,
    
          "modelConv2D_1_lessFilters_noMaxPool":modelConv2D_1_lessFilters_noMaxPool,
    "modelConv2D_1_lessFilters_deeper_noMaxPool":modelConv2D_1_lessFilters_deeper_noMaxPool,
    
          "modelConv2D_1_lessFilters_deeper_lessMaxPool":modelConv2D_1_lessFilters_deeper_lessMaxPool,
    "modelConv2D_simple":modelConv2D_simple,
    "modelConv2D_1Conv":modelConv2D_1Conv,
    "modelConv2D_1Conv_nopadding":modelConv2D_1Conv_nopadding,
    "modelConv2D_1Conv_nopadding_mu60_hh4b":modelConv2D_1Conv_nopadding,
    "modelConv2D_1Conv_nopadding_mu200_hh4b":modelConv2D_1Conv_nopadding,
    
          "modelConv2D_1Conv_nopadding_mu200_hh4b_1filters_3x3":modelConv2D_1Conv_nopadding_1filters_3x3,
    "modelConv2D_1Conv_nopadding_mu200_hh4b_2filters_3x3":modelConv2D_1Conv_nopadding_2filters_3x3,
    "modelConv2D_1Conv_nopadding_mu200_hh4b_3filters_3x3":modelConv2D_1Conv_nopadding_3filters_3x3,
    "modelConv2D_1Conv_nopadding_mu200_hh4b_4filters_3x3":modelConv2D_1Conv_nopadding_4filters_3x3,

          "modelConv2D_1Conv_nopadding_mu60_hh4b_1filters_3x3":modelConv2D_1Conv_nopadding_1filters_3x3,
    "modelConv2D_1Conv_nopadding_mu60_hh4b_2filters_3x3":modelConv2D_1Conv_nopadding_2filters_3x3,
    "modelConv2D_1Conv_nopadding_mu60_hh4b_3filters_3x3":modelConv2D_1Conv_nopadding_3filters_3x3,
    "modelConv2D_1Conv_nopadding_mu60_hh4b_4filters_3x3":modelConv2D_1Conv_nopadding_4filters_3x3,

          "modelConv2D_1Conv_nopadding_mu200_hh4b_1filters_7x7":modelConv2D_1Conv_nopadding_1filters_7x7,
    "modelConv2D_1Conv_nopadding_mu200_hh4b_1filters_9x9":modelConv2D_1Conv_nopadding_1filters_9x9,
    "modelConv2D_1Conv_nopadding_mu200_hh4b_5filters_7x7":modelConv2D_1Conv_nopadding_5filters_7x7,

          "modelConv2D_1Conv_nopadding_mu200_1filters_3x3":modelConv2D_1Conv_nopadding_1filters_3x3,
    "modelConv2D_1Conv_nopadding_mu200_2filters_3x3":modelConv2D_1Conv_nopadding_2filters_3x3,
    "modelConv2D_1Conv_nopadding_mu200_2filters_3x3_8fc":modelConv2D_1Conv_nopadding_2filters_3x3_8fc,
    "modelConv2D_1Conv_nopadding_mu200_3filters_3x3":modelConv2D_1Conv_nopadding_3filters_3x3,
    "modelConv2D_1Conv_nopadding_mu200_4filters_3x3":modelConv2D_1Conv_nopadding_4filters_3x3,

          "modelConv2D_1Conv_nopadding_mu200_1filters_7x7":modelConv2D_1Conv_nopadding_1filters_7x7,
    "modelConv2D_1Conv_nopadding_mu200_1filters_9x9":modelConv2D_1Conv_nopadding_1filters_9x9,

          "modelConv2D_1Conv_nopadding_mu200_5filters_7x7":modelConv2D_1Conv_nopadding_5filters_7x7,
    "modelConv2D_1Conv_nopadding_mu200_5filters_11x11":modelConv2D_1Conv_nopadding_5filters_11x11,
    "modelConv2D_1Conv_nopadding_mu200_11filters_11x11":modelConv2D_1Conv_nopadding_11filters_11x11,
    "modelConv2D_1Conv_nopadding_mu200_9filters_9x9":modelConv2D_1Conv_nopadding_9filters_9x9,
    "modelConv2D_1Conv_nopadding_mu200_20filters_9x9":modelConv2D_1Conv_nopadding_20filters_9x9,
    "modelConv2D_1Conv_nopadding_mu200_9filters_9x9_stride5x5":modelConv2D_1Conv_nopadding_9filters_9x9_stride5x5,
    "modelConv2D_1Conv_nopadding_mu200_4filters_9x9_stride2x2":modelConv2D_1Conv_nopadding_4filters_9x9_stride2x2,
    "modelConv2D_1Conv_nopadding_mu200_4filters_9x9":modelConv2D_1Conv_nopadding_4filters_9x9,

          "modelConv2D_1Conv_nopadding_stride2x2":modelConv2D_1Conv_nopadding_stride2x2,
    "modelConv2D_1Conv_stride2":modelConv2D_1Conv_stride2,
    "modelConv2D_1Conv_5x5":modelConv2D_1Conv_5x5,
    "modelConv2D_1Conv_5x5_stride2":modelConv2D_1Conv_5x5_stride2,
    "modelConv2D_1Conv_7x7":modelConv2D_1Conv_7x7,

          "modelConv2D_inception":modelConv2D_inception,
    "modelConv2D_inception_no1x1":modelConv2D_inception_no1x1,
    "modelConv2D_inception_no1x1_5_7_11":modelConv2D_inception_no1x1_5_7_11,
    "modelConv2D_inception_no1x1_5_7_11_19":modelConv2D_inception_no1x1_5_7_11_19,

          "modelConv2D_inception_upTo11":modelConv2D_inception_upTo11,
    "modelConv2D_inception_upTo11_no1x1":modelConv2D_inception_upTo11_no1x1,
    "modelConv2D_inception_32Filters":modelConv2D_inception_32Filters,
    "modelConv2D_inception_noMaxPool":modelConv2D_inception_noMaxPool,
    "modelConv2D_inception_32Filters_noMaxPool":modelConv2D_inception_32Filters_noMaxPool,
    #"modelConv2D_inception_32Filters_deeper":modelConv2D_inception_32Filters_deeper,
          "modelConv2D_simple_prelu":modelConv2D_simple_prelu,
    "modelMLP_1":modelMLP_1,
    "modelMLP_2":modelMLP_2,
    "modelMLP_3":modelMLP_3,
    "modelMLP_1FourVec_1":modelMLP_1,
    "modelMLP_4FourVec_1":modelMLP_1,
    "modelMLP_6FourVec_1":modelMLP_1,
    "modelMLP_8FourVec_1":modelMLP_1,
    "modelMLP_10FourVec_1":modelMLP_1,
}

for modelName in models.keys():
    if "Conv2D" in modelName:
        models[modelName.replace("Conv2D", "Conv2DGFEX")] = models[modelName]


