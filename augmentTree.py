#!/usr/bin/env python

from ROOT import TFile, TTree, TClonesArray, gROOT, gSystem, vector, TVector2, TObject
from array import array
import os, argparse, math
TTree.SetMaxTreeSize( 1000000000000 );

def augmentTree(inFPath, inTreeName='mytree', outTreeName='mytreeNew'):
    """ Function that creates a friend tree with augmented information based on the original tree. """
    inF = TFile.Open(inFPath, 'update')
    inTree = inF.Get(inTreeName)
    newTree = TTree(outTreeName, "")
    newTree.AddFriend(inTree);

    minPtCut = 20000
    
    AntiKt4EMTopoNJets               = array('i', [0])
    AntiKt4EMTopoHt                  = array('f', [0])
    AntiKt4EMTopoMaxDeltaR           = array('f', [0])
    AntiKt4EMTopoMaxDeltaPhi         = array('f', [0])
    AntiKt4EMTopoMaxDeltaEta         = array('f', [0])
    AntiKt4EMTopoLeadSubleadDeltaR   = array('f', [0])
    AntiKt4EMTopoLeadSubleadDeltaPhi = array('f', [0])
    AntiKt4EMTopoLeadSubleadDeltaEta = array('f', [0])
    AntiKt4EMTopoJetPt               = vector(float)()
    AntiKt4EMTopoJetEta              = vector(float)()
    AntiKt4EMTopoJetPhi              = vector(float)()
    
    AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsNJets               = array('i', [0])
    AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsHt                  = array('f', [0])
    AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsMaxDeltaR           = array('f', [0])
    AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsMaxDeltaPhi         = array('f', [0])
    AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsMaxDeltaEta         = array('f', [0])
    AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsLeadSubleadDeltaR   = array('f', [0])
    AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsLeadSubleadDeltaPhi = array('f', [0])
    AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsLeadSubleadDeltaEta = array('f', [0])
    AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsJetPt               = vector(float)()
    AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsJetEta              = vector(float)()
    AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsJetPhi              = vector(float)()
    AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsJetM                = vector(float)()

    newTree.Branch("NJetsAntiKt4EMTopoPt20", AntiKt4EMTopoNJets, "NJetsAntiKt4EMTopoPt20/I")
    newTree.Branch("HtAntiKt4EMTopoPt20", AntiKt4EMTopoHt, "HtAntiKt4EMTopoPt20/F")
    newTree.Branch("MaxDeltaPhiAntiKt4EMTopoPt20", AntiKt4EMTopoMaxDeltaPhi, "MaxDeltaPhiAntiKt4EMTopoPt20/F")
    newTree.Branch("MaxDeltaEtaAntiKt4EMTopoPt20", AntiKt4EMTopoMaxDeltaEta, "MaxDeltaEtaAntiKt4EMTopoPt20/F")
    newTree.Branch("MaxDeltaRAntiKt4EMTopoPt20", AntiKt4EMTopoMaxDeltaR, "MaxDeltaRAntiKt4EMTopoPt20/F")
    
    newTree.Branch("LeadSubleadDeltaPhiAntiKt4EMTopoPt20", AntiKt4EMTopoLeadSubleadDeltaPhi, "LeadSubleadDeltaPhiAntiKt4EMTopoPt20/F")
    newTree.Branch("LeadSubleadDeltaEtaAntiKt4EMTopoPt20", AntiKt4EMTopoLeadSubleadDeltaEta, "LeadSubleadDeltaEtaAntiKt4EMTopoPt20/F")
    newTree.Branch("LeadSubleadDeltaRAntiKt4EMTopoPt20", AntiKt4EMTopoLeadSubleadDeltaR, "LeadSubleadDeltaRAntiKt4EMTopoPt20/F")
    newTree.Branch("AntiKt4EMTopoPt20_pt", AntiKt4EMTopoJetPt)
    newTree.Branch("AntiKt4EMTopoPt20_eta", AntiKt4EMTopoJetEta)
    newTree.Branch("AntiKt4EMTopoPt20_phi", AntiKt4EMTopoJetPhi)

    newTree.Branch("NJetsAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20",
                   AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsNJets, "NJetsAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20/I")
    newTree.Branch("HtAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20",
                   AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsHt, "HtAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20/F")
    newTree.Branch("MaxDeltaPhiAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20",
                   AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsMaxDeltaPhi, "MaxDeltaPhiAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20/F")
    newTree.Branch("MaxDeltaEtaAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20",
                   AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsMaxDeltaEta, "MaxDeltaEtaAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20/F")
    newTree.Branch("MaxDeltaRAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20",
                   AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsMaxDeltaR, "MaxDeltaRAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20/F")
    newTree.Branch("LeadSubleadDeltaPhiAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20",
                   AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsLeadSubleadDeltaPhi,
                   "LeadSubleadDeltaPhiAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20/F")
    newTree.Branch("LeadSubleadDeltaEtaAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20",
                   AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsLeadSubleadDeltaEta,
                   "LeadSubleadDeltaEtaAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20/F")
    newTree.Branch("LeadSubleadDeltaRAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20",
                   AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsLeadSubleadDeltaR,
                   "LeadSubleadDeltaRAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20/F")
    
    newTree.Branch("AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20_pt", AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsJetPt)
    newTree.Branch("AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20_eta", AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsJetEta)
    newTree.Branch("AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20_phi", AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsJetPhi)
    newTree.Branch("AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20_m", AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsJetM)

    count  = 0;
    nEntries = inTree.GetEntries()
    for evt in inTree:
        # if count > 1000:
        #     break;
        if count % 100 == 0:
            print "Processed", count, "of", nEntries
        count+=1
        jetCount = 0

        AntiKt4EMTopoNJets[0]               = 0
        AntiKt4EMTopoHt[0]                  = 0
        AntiKt4EMTopoMaxDeltaR[0]           = 0
        AntiKt4EMTopoMaxDeltaPhi[0]         = 0
        AntiKt4EMTopoMaxDeltaEta[0]         = 0
        AntiKt4EMTopoLeadSubleadDeltaR[0]   = -9999
        AntiKt4EMTopoLeadSubleadDeltaPhi[0] = -9999
        AntiKt4EMTopoLeadSubleadDeltaEta[0] = -9999
        AntiKt4EMTopoJetPt.clear()
        AntiKt4EMTopoJetPhi.clear()
        AntiKt4EMTopoJetEta.clear()

        AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsNJets[0]               = 0
        AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsHt[0]                  = 0
        AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsMaxDeltaR[0]           = 0
        AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsMaxDeltaPhi[0]         = 0
        AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsMaxDeltaEta[0]         = 0
        AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsLeadSubleadDeltaR[0]   = -9999
        AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsLeadSubleadDeltaPhi[0] = -9999
        AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsLeadSubleadDeltaEta[0] = -9999
        AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsJetPt.clear()
        AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsJetPhi.clear()
        AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsJetEta.clear()
        AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsJetM.clear()
        
        for jetI in range(evt.CalibratedAntiKt4EMTopoJets_n):
            if evt.CalibratedAntiKt4EMTopoJets_pt.at(jetI) < minPtCut:
                continue
            AntiKt4EMTopoJetPt.push_back(evt.CalibratedAntiKt4EMTopoJets_pt.at(jetI))
            AntiKt4EMTopoJetPhi.push_back(evt.CalibratedAntiKt4EMTopoJets_phi.at(jetI))
            AntiKt4EMTopoJetEta.push_back( evt.CalibratedAntiKt4EMTopoJets_eta.at(jetI))
            AntiKt4EMTopoNJets[0] += 1
            AntiKt4EMTopoHt[0] += evt.CalibratedAntiKt4EMTopoJets_pt.at(jetI)

            for jetI2 in range(jetI, evt.CalibratedAntiKt4EMTopoJets_n):
                tempDEta = math.sqrt((evt.CalibratedAntiKt4EMTopoJets_eta.at(jetI)-evt.CalibratedAntiKt4EMTopoJets_eta.at(jetI2))**2)
                tempDPhi = abs(TVector2.Phi_mpi_pi(evt.CalibratedAntiKt4EMTopoJets_phi.at(jetI)-evt.CalibratedAntiKt4EMTopoJets_phi.at(jetI2)))
                tempDR = math.sqrt(tempDPhi**2+tempDEta**2)
                if tempDR > AntiKt4EMTopoMaxDeltaR[0]:
                    AntiKt4EMTopoMaxDeltaR[0] = tempDR
                if tempDPhi > AntiKt4EMTopoMaxDeltaPhi[0]:
                    AntiKt4EMTopoMaxDeltaPhi[0] = tempDPhi
                if tempDEta > AntiKt4EMTopoMaxDeltaEta[0]:
                    AntiKt4EMTopoMaxDeltaEta[0] = tempDEta
        
        if  AntiKt4EMTopoNJets[0] >= 2:
            AntiKt4EMTopoLeadSubleadDeltaPhi[0] = abs(TVector2.Phi_mpi_pi(AntiKt4EMTopoJetPhi.at(0)-AntiKt4EMTopoJetPhi.at(1)))
            AntiKt4EMTopoLeadSubleadDeltaEta[0] = math.sqrt((AntiKt4EMTopoJetEta.at(0)-AntiKt4EMTopoJetEta.at(1))**2)
            AntiKt4EMTopoLeadSubleadDeltaR[0] = math.sqrt(AntiKt4EMTopoLeadSubleadDeltaPhi[0]**2+AntiKt4EMTopoLeadSubleadDeltaEta[0]**2)

        for jetI in range(evt.AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJets_n):
            if evt.AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJets_pt.at(jetI) < minPtCut:
                continue
            AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsJetPt.push_back(evt.AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJets_pt.at(jetI))
            AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsJetPhi.push_back(evt.AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJets_phi.at(jetI))
            AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsJetEta.push_back(evt.AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJets_eta.at(jetI))
            AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsJetM.push_back(evt.AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJets_m.at(jetI))
            AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsNJets[0] += 1
            AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsHt[0] += evt.AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJets_pt.at(jetI)

            for jetI2 in range(jetI, evt.AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJets_n):
                tempDEta = math.sqrt((evt.AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJets_eta.at(jetI)-evt.AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJets_eta.at(jetI2))**2)
                tempDPhi = abs(TVector2.Phi_mpi_pi(evt.AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJets_phi.at(jetI)-evt.AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJets_phi.at(jetI2)))
                tempDR = math.sqrt(tempDPhi**2+tempDEta**2)
                if tempDR > AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsMaxDeltaR[0]:
                    AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsMaxDeltaR[0] = tempDR
                if tempDPhi > AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsMaxDeltaPhi[0]:
                    AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsMaxDeltaPhi[0] = tempDPhi
                if tempDEta > AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsMaxDeltaEta[0]:
                    AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsMaxDeltaEta[0] = tempDEta
        
        if  AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsNJets[0] >= 2:
            AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsLeadSubleadDeltaPhi[0] = abs(TVector2.Phi_mpi_pi(AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsJetPhi.at(0)- AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsJetPhi.at(1)))
            AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsLeadSubleadDeltaEta[0] = math.sqrt((AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsJetEta.at(0)**2- AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsJetEta.at(1))**2)
            AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsLeadSubleadDeltaR[0] = math.sqrt(AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsLeadSubleadDeltaPhi[0]**2+AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsLeadSubleadDeltaEta[0]**2)

               
        newTree.Fill()
    newTree.Write(outTreeName,TObject.kOverwrite)
    inF.Close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Augment a tree with additional information.')
    parser.add_argument('inFPath', type=str, help='Location of the input file')#, default="/projects/chep/whopkins/gFEXSamps/user.whopkins.12579782.OUTPUT._000002.root")

    args = parser.parse_args() 
    augmentTree(args.inFPath);
    
