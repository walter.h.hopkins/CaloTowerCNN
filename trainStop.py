#!/usr/bin/env python
"""@package docstring
This module preps the data and trains and tests various NNs
"""

import sys, os, argparse, matplotlib, cutStrings
import numpy as np
np.random.seed(19)

matplotlib.use('Agg')
import matplotlib.pyplot as plt

from keras.models import load_model
from keras import backend as K
K.set_image_dim_ordering('th')

from stopModels import models
from MLUtils import *

# Branches used for training
branchSets = {
    "stopVars":["Met", ("AntiKt12M[0]",0,1), ("AntiKt12M[1]",0,1), ("AntiKt8M[0]",0,1), "MtBMin", "MtBMax", "NBJets", "DRBB", "MtTauCand"],
    "stopFourVecBTag":["JetPt", "JetIsBTagged", "JetEta", "JetPhi"],
    "stopFourVec":["JetPt","JetEta","JetPhi"],
    }
# Set of branches to also grab for comaprisons. The branch name in a tuple (if accessing a vector) or string comes first. Then the scaling, binning and units.
extraBranchInfo = {"stopFourVec":[(('AntiKt12M[0]',0,1), [i*20 for i in range(31)], 1., 'GeV'),
                                  (('AntiKt12M[1]',0,1), [i*20 for i in range(31)], 1., 'GeV'),
                                  (('AntiKt8M[0]',0,1), [i*20 for i in range(31)], 1., 'GeV'),
                                  (('AntiKt8M[1]',0,1), [i*20 for i in range(31)], 1., 'GeV'),
                                  ("MtBMin", [i*10 for i in range(21)], 1., 'GeV'),
                                  ("MtBMax", [i*10 for i in range(21)], 1., 'GeV'),
                                  ("NBJets", [i*10 for i in range(21)], 1., ''),
                                  ("NJets", [i*10 for i in range(21)], 1., ''),
                                  ("Met", [i*10 for i in range(21)], 1., 'GeV'),
                                  ("DRBB", [i*10 for i in range(21)], 1., ''),
                                  ("MtTauCand", [i*10 for i in range(21)], 1., 'GeV'),              
                                  ("dsid", [i for i in range(21)], 1., '-364140'),              
                   ],
                 }

extraBranchSets = {}
for key in extraBranchInfo:
    extraBranchSets[key] = []
    for branchInfo in extraBranchInfo[key]:
        extraBranchSets[key].append(branchInfo[0])

availEtaBins = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.5, 2.7, 2.9, 3.1, 3.2, 3.5, 4.0, 4.45, 4.9]
phiGran = 0.2
  
                

def trainDL2D(modelNames, modelFPath, sigFName, bkgFNames, epochs=20, batchSize=32, nTrainSamp=20000, nTestSamp=1000, precutKey='NoCut', maxEta=2.8, sigTreeName='stop_0Lep', bkgTreeName='stop_0Lep', sigFriendName='stop_0LepExt', bkgFriendName='stop_0LepExt', branches='stopFourVecBTag', extraBranchKey='', weightBranches=["ThreeBodyWeightNum","JVTSF","sherpaNJetsWeight","BTagSF","PileupWeight","FinalXSecWeight","GenWeight"], suffix="", shortSigName='signal', shortBkgName='bkg', update=False):

     # The granularity in the gFEX isn't entirely uniform in eta
    etaBins = [etaBin for etaBin in availEtaBins if etaBin <= maxEta]
    etaBins = [-1.*etaBins[-1*i] for i in range(1,len(etaBins))]+etaBins
    
    nEtaPix = len(etaBins)-1
    nPhiPix = int(round(6.4/phiGran))
    dPhi = (2*math.pi)/nPhiPix
    phiBins = [-1*math.pi+i*dPhi for i in range(nPhiPix+1)]

    maxEtaStr = "_%.2f" % maxEta
    
    if extraBranchKey in extraBranchSets:
        tempExtraBranches = extraBranchSets[extraBranchKey]
    else:
        tempExtraBranches = []
    
    (xTrain, yTrain, xTest, yTest, weightsTrain, weightsTest, extraVarsTrain, extraVarsTest, sigGrid, bkgGrid, sigExtraVars, bkgExtraVars, sigExtraVarsSubset, totalBkgExtraVarsSubset, sigWeights, bkgWeights) = prepROOTDataML2D(nTrainSamp, nTestSamp, branchSets[branches], sigFName, bkgFNames, etaBins, phiBins, precutKey=precutKey, suffix='stop'+maxEtaStr, weightBranches=weightBranches, sigTreeName=sigTreeName, bkgTreeName=bkgTreeName, sigFriendName=sigFriendName, bkgFriendName=bkgFriendName, extraBranches=tempExtraBranches, shortSigName=shortSigName, shortBkgName=shortBkgName, update=update)
     
    print "Training with model", modelNames
    
    for modelName in modelNames:
        model = models[modelName]((len(branchSets[branches])-2,nEtaPix,nPhiPix), modelName, modelFPath)
        model.fit(xTrain, yTrain, epochs=epochs, batch_size=batchSize, shuffle=False,sample_weight=weightsTrain, validation_data=(xTest,yTest))
        # Store model to file
        model.save(modelFPath+modelName+'.h5')
        scores = model.evaluate(xTrain, yTrain,sample_weight=weightsTrain)
        scoresTest = model.evaluate(xTest, yTest,sample_weight=weightsTest)
        model.summary()

        print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))
        print("\nTest sample, %s: %.2f%%" % (model.metrics_names[1], scoresTest[1]*100))
        
def testModel(sigFName, bkgFNames, modelFiles, nTrainSamp=10000, nTestSamp=10000, precutKey='NoCut', compRegions=['SRB'], weightStr="1", maxEta=2.8, sigTreeName='stop_0Lep', bkgTreeName='stop_0Lep', sigFriendName='stop_0LepExt', bkgFriendName='stop_0LepExt', branches='stopFourVecBTag', extraBranchKey='', weightBranches=["ThreeBodyWeightNum","JVTSF","sherpaNJetsWeight","BTagSF","PileupWeight","FinalXSecWeight","GenWeight"], shortSigName='signal', shortBkgName='bkg', update=False, dB=0.15, lumi=36):
    """ This function produces various figures of merits for the trained model such as training and testing output, trained weights,
    and turn-on curves."""
    
    # The granularity in the gFEX isn't entirely uniform in eta
    etaBins = [etaBin for etaBin in availEtaBins if etaBin <= maxEta]
    etaBins = [-1.*etaBins[-1*i] for i in range(1,len(etaBins))]+etaBins

    nEtaPix = len(etaBins)-1
    nPhiPix = int(round(6.4/phiGran))
    dPhi = (2*math.pi)/nPhiPix
    phiBins = [-1*math.pi+i*dPhi for i in range(nPhiPix+1)]

    maxEtaStr = "_%.2f" % maxEta

    if extraBranchKey in extraBranchSets:
        tempExtraBranches = extraBranchSets[extraBranchKey]
    else:
        tempExtraBranches = []
        
    testTrainInfo = prepROOTDataML2D(nTrainSamp, nTestSamp, branchSets[branches], sigFName, bkgFNames, etaBins, phiBins, precutKey=precutKey, suffix='stop'+maxEtaStr, weightBranches=weightBranches, sigTreeName=sigTreeName, bkgTreeName=bkgTreeName, sigFriendName=sigFriendName, bkgFriendName=bkgFriendName, extraBranches=tempExtraBranches, shortSigName=shortSigName, shortBkgName=shortBkgName, update=update)

    yieldInfo = {}
    weightStr = "("+"*".join(weightBranches)+"*1000*"+str(lumi)+")"
    for compRegion in compRegions:
        compRegionCut = "("+cutStrings.allCuts[compRegion]+")*("+weightStr+")"
        yieldInfo[compRegion] = getSigBkgEffFromCut(compRegionCut, sigFName, bkgFNames, precutKey="("+cutStrings.allCuts[precutKey]+")*("+weightStr+")")
        
    testTrainData = testTrainInfo[:4]
    testTrainWeights = testTrainInfo[4:6]
    extraVarsTrain,extraVarsTest = testTrainInfo[6:8]
    allData = testTrainInfo[8:10]
    extraVars = testTrainInfo[10:12]
    allWeights = testTrainInfo[-2:]
    models = {}
    sigBkgEffs= {}
    bkgNNCut = 0.2
    for modelFile in modelFiles:
        modelName = modelFile.split("/")[-1]
        print "Fetching", modelName
        model = load_model(modelFile+".h5")
        model.summary()
        models[modelName] = model
        
        print "Getting signal and background efficiency for", modelName
        responseFName = 'response_'+modelName+'.pkl'
        (responses, sigBkgEffs[modelName]) = getSigBkgEffFromModel(model, testTrainData, testTrainWeights, allData, allWeights, responseFName, binning=[i*.001 for i in range(1001)], update=update)
        
        testTrainResponses = responses[:4]
        allResponses = responses[4:]
        
        #plotResponse(testTrainResponses, suffix=modelName)
        
        # # print "Making rate plots."

        # # print "Plotting weights and making input images that maximimally activate a filter."
        #plotWeights(model, 0, suffix='_'+modelName)
        #plotFinalWeights(model, suffix='_'+modelName)
        #deepDraw(model, nXBins=nEtaPix, nYBins=nPhiPix, suffix='_'+modelName)
        #findInputMaxMinAct(model, allData, extraVars, extraBranchInfo[extraBranchKey], 0, nImages=5)
        #makePseudoData(model, nXBins=nEtaPix, nYBins=nPhiPix)

    sigEffsTrain = {}
    bkgEffsTrain = {}

    print "Plotting response for", modelName
    for modelName in models:
        sigEffsTrain[modelName.replace('modelConv2D','CNN')] = sigBkgEffs[modelName][2]
        bkgEffsTrain[modelName.replace('modelConv2D','CNN')] = sigBkgEffs[modelName][3]

    for region in yieldInfo:
        sigEffsTrain[region] = [yieldInfo[region][4]]
        bkgEffsTrain[region] = [yieldInfo[region][5]]

    sigPrecutYield = yieldInfo.values()[0][0];
    bkgPrecutYield = yieldInfo.values()[0][1];
    plotSignifVsEff(sigPrecutYield, bkgPrecutYield, sigEffsTrain, bkgEffsTrain, db=dB)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train a CNN for stop signal/background discrimination.')
    parser.add_argument('--sigFName', type=str, help='Location of signal file', default='/projects/chep/whopkins/stopSamples/withSyst/mc15_13TeV.387168.MadGraphPythia8EvtGen_A14NNPDF23LO_TT_directTT_600_300.merge.DAOD_SUSY1.e3969_a766_a821_r7676_p2666Ext.root')
    parser.add_argument('--bkgFNames', type=str, help='Location of background files', default='/projects/chep/whopkins/stopSamples/withSyst/ZSherpa221Ext.root')

    parser.add_argument('--sigTreeName', type=str, help='Signal tree name', default='stop_0Lep')
    parser.add_argument('--bkgTreeName', type=str, help='Background tree name', default='stop_0Lep')
    
    parser.add_argument('--test', action='store_true', help="Don't train, test")
    parser.add_argument('--update', action='store_true', help="Update h5 files by redoing ROOT conversion.")
    parser.add_argument('--branches', type=str, default="stopFourVec", help='Key to use for branch dictionary')
    parser.add_argument('--extraBranchKey', type=str, default="stopFourVec", help='Key to use for extra branches to be used (not for training but for studies)')

    parser.add_argument('--shortSigName', type=str, default='stop_600_300', help='Short name used to describe signal h5 file.')
    parser.add_argument('--shortBkgName', type=str, default='zJets', help='Short name used to describe background h5 file.')

    parser.add_argument('--modelNames', type=str, help='Name of models, comma delimited.')
    parser.add_argument('--modelFPath', type=str, default='/projects/chep/whopkins/stopFiles/', help='Path where model should be saved.')
    parser.add_argument('--nTrainSamp', type=int, help='Number of events to use for training', default=20000)
    parser.add_argument('--nTestSamp', type=int, help='Number of events to use for testing', default=5000)
    parser.add_argument('--precutKey', type=str, help='Preselection cut to use', default='precutSR')
    parser.add_argument('--maxEta', type=float, help='Maximum eta', default=2.8)
    parser.add_argument('--epochs', type=int, help='Number of epochs', default=20)
    parser.add_argument('--suffix', type=str, help='suffix to identify different grid files and model files', default="")
    parser.add_argument('--summaryOnly', action='store_true', help='Only print the summary of a model', default=False)
    parser.add_argument('--dB', type=float, help='Background uncertainty', default=0.15)

    args = parser.parse_args() 
    bkgFNames = [bkgFName for bkgFName in args.bkgFNames.split(',')]
   
    if args.test:
        testModel(args.sigFName, bkgFNames, [args.modelFPath+"/"+modelName for modelName in args.modelNames.split(",")], nTrainSamp=args.nTrainSamp, nTestSamp=args.nTestSamp, precutKey=args.precutKey, maxEta=args.maxEta, branches=args.branches, extraBranchKey=args.extraBranchKey, shortSigName=args.shortSigName, shortBkgName=args.shortBkgName, update=args.update, dB=args.dB);
    elif args.summaryOnly:
        for modelFile in [args.modelFPath+"/"+modelName for modelName in args.modelNames.split(",")]:
            modelName = modelFile.split("/")[-1]
            print "Fetching", modelName
            model = load_model(modelFile+".h5")
            model.summary()
    else:
        modelNames = args.modelNames.split(",")   
        trainDL2D(modelNames, args.modelFPath, args.sigFName, bkgFNames, epochs=args.epochs, nTrainSamp=args.nTrainSamp, nTestSamp=args.nTestSamp, maxEta=args.maxEta, branches=args.branches, extraBranchKey=args.extraBranchKey, precutKey=args.precutKey, shortSigName=args.shortSigName, shortBkgName=args.shortBkgName, update=args.update);

