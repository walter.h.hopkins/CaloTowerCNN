# On lxplus or acas
#setupATLAS -q
#lsetup "root 6.10.04-x86_64-slc6-gcc62-opt"
export PATH=$HOME/.local/bin:$PATH

# for talapas
module load anaconda2/4.4.0 cuda cmake
source /projects/chep/whopkins/root/bin/thisroot.sh
