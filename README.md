# Deep learning for calorimeter trigger (Phase I and II)

This package is meant to study deep learning using calorimeter towers to classify mininum bias and multijet events originating from higgs production. The two main components are trainDL.py which trains and tests models and MLUtils.py which contains various functions to process input data and assess the performance of the convoloutional algorithm.

## Installation
Coming soon!

## Setup
Before you start from a new terminal source `setup.sh`.

## To do
* Understand dependence on mu
