#!/usr/bin/env python

import argparse, math, glob

from ROOT import TH1F, TFile, TTree, TChain, TCanvas, gROOT, TLegend, TH2F, TEventList, gDirectory
gROOT.SetBatch(True)

from setStyle import setStyle, CompLegend

def plotGTowerGrid(inFPath, treeName, mapTreeName, eventNum):
    """ Plot eta/phi grid for a particular event. """
    gROOT.SetBatch(True)
    setStyle()
    gROOT.ForceStyle()
    setStyle()
    
    firstName = glob.glob(inFPath)[0]
    
    chain = TChain(treeName)
    chain.Add(inFPath)

    inF     = TFile.Open(firstName)
    mapTree = inF.Get(mapTreeName)

    chain.Draw(">>elist", "eventNumber=="+str(eventNum));
    elist = gDirectory.Get("elist");
    entryNum = elist.GetEntry(0)

    h2 = TH2F('grid', '', 32, -3.2, 3.2, 32, -1*math.pi, math.pi)
    chain.GetEntry(entryNum)
    mapTree.GetEntry(0)
    for gTowerI in range(len(chain.gTowerEt)):
        gTowerEt = chain.gTowerEt.at(gTowerI)/1000.
        gTowerEta = mapTree.gTowerEtaGeometric.at(gTowerI)
        gTowerPhi = mapTree.gTowerPhiGeometric.at(gTowerI)
        h2.Fill(gTowerEta, gTowerPhi, gTowerEt)
    c = TCanvas('c', '', 800, 600);
    p = c.cd()
    p.SetLogz()
    h2.Draw('colz')
    h2.GetXaxis().SetTitle("#eta")
    h2.GetYaxis().SetTitle("#phi")
    h2.GetZaxis().SetTitle("gTowerEt [GeV]")
    c.Print('grid.pdf')
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Plot gTower related variables.')
    parser.add_argument('--inFPath', type=str, help='Location of input file', default='/projects/chep/whopkins/gFEXSamps/user.whopkins.301323.Pythia8EvtGen_A14NNPDF23LO_zprime500_tt.recon.AOD.e5905_s3142_s3143_r9589.noSCProv.v31_OUTPUT/*.root')
    parser.add_argument('--treeName', type=str, help='treeName', default='mytree')
    parser.add_argument('--mapTreeName', type=str, help='Name of map tree that contains eta/phi coordinates of gTowers', default='maps')
    parser.add_argument('--eventNum', type=int, help='Event to plot', default=1)

    args = parser.parse_args()
    
    plotGTowerGrid(args.inFPath, args.treeName, args.mapTreeName, args.eventNum)
