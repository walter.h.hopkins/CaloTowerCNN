#!/usr/bin/env python

from ROOT import TFile
from root_pandas import read_root
import pandas as pd
import numpy as np
import argparse, re, itertools, collections, math

def flatten(df):
    """! 
    Flatten a series by making new columns for all the array elements. 
    This assumes that the only type of object are numpy arrays and not something more complex.
    """
    allColNames = []
    cols = []
    for col in df.keys():
        if df[col].apply(lambda y: type(y) == np.ndarray).any():
            cols.append(col)
        else:
            allColNames.append(col)
            
    while len(cols) > 0:
        for col in cols:
            series = df[col].apply(lambda y: [] if y is None else y)
            newSeries = series.apply(lambda y: y.tolist() if type(y) == np.ndarray else y)
            tempDF = pd.DataFrame(newSeries.values.tolist(), index=df.index);
            
            if tempDF.shape[1] == 0:
                #print col, "is empty. Assigning NaN.", tempDF.shape
                df.drop([col], axis=1, inplace=True)
                continue
            newColNames = [col+'_'+str(i) for i in range(tempDF.shape[1])]
            allColNames.extend(newColNames);
            df[newColNames] = tempDF
            df.drop([col], axis=1, inplace=True)
        newColNames = flatten(df);
        allColNames.extend(newColNames);
        cols = []
        for col in df.keys():
            if df[col].apply(lambda y: type(y) == np.ndarray).any():
                cols.append(col)
    return allColNames

def getMaxLength(tree, vecBranch, chunkSize=100000):
    """!
    Get the maximum value of a vector branch by drawing it chunks so that we don't fill up the memory.
    """
    totalEvents = tree.GetEntries();
    tempMaxLengths = []
    for chunkI in range(int(math.ceil(1.0*totalEvents/chunkSize))):
        firstEntry = chunkSize*chunkI;
        lastEntry = (chunkI+1)*chunkSize;
        nEntries = tree.Draw("Length$("+vecBranch+")", "", "goff", firstEntry, lastEntry)
        lengthsArray = tree.GetV1()
        lengths = [lengthsArray[i] for i in xrange(nEntries)]
        if len(lengths) > 0:
            maxLength = max(lengths)
            tempMaxLengths.append(maxLength)
        
    return int(max(tempMaxLengths))

def getFlatColNames(tree, vecBranch, depth, chunkSize=100000):
    """!
    Get the flat column names from branch name. Only handles depths up to 2 for vectors.
    Getting the std vector lengths could be done recursively. Getting the deepest one is easy but didn't 
    have enough time to figure out intermediate depths. 
    """
    maxLengths = [getMaxLength(tree, vecBranch, chunkSize=chunkSize)]
    if depth > 1:
        allLengths = []
        for elI in range(maxLengths[0]):
            allLengths.append(getMaxLength(tree, vecBranch+"["+str(elI)+"]", chunkSize=chunkSize))
        if len(allLengths) > 0:
            newMaxLength = int(max(allLengths))
            maxLengths.append(newMaxLength)
    ranges = [range(maxLength) for maxLength in maxLengths]
    product = itertools.product(*ranges)
    colNames = []
    for elSet in product:
        colNames.append(vecBranch+"_"+"_".join([str(el) for el in elSet]))
    return colNames

def convertTree(inFName, outFName, tree, chunkSize, branchVetoList=[]):
    """! Convert tree to pandas dataframe after flattening all branches."""
    
    typeConv = {
        'double':'float64',
        'float':'float32',
        'int':'int32',
        'long64':'int64',
        'ulong64':'int64',
        'bool':'int8',
    } 
    totalEvents = tree.GetEntries()
    if totalEvents< 1:
        return;

    # Find the branches that are vectors
    leaves = tree.GetListOfLeaves()
    branches = []
    branchTypes = {}
    branchBaseTypes = {}
    vecBranches = []
    vecDepths = {}
    flatBranches = []
    for leaf in leaves:
        leafName = leaf.GetName()
        leafTypeName = leaf.GetTypeName()
        if not all([not re.search(patt, leafName) for patt in branchVetoList]):
            continue;
        branches.append(leafName)
        leafType = leafTypeName.replace('vector', '').replace('>', '').replace('<', '').replace('_t', '').lower().lstrip().rstrip()
        branchBaseTypes[leafName] = typeConv[leafType]
        branchTypes[leafName] = leafTypeName
        if 'vector' in leafTypeName:
            vecBranches.append(leafName);
            vecDepths[leafName] = leafTypeName.count('vector')
        else:
            flatBranches.append(leafName);

    evtCount = 0
    colNames = flatBranches

    for vecBranch in vecBranches:
        tempColNames = getFlatColNames(tree, vecBranch, vecDepths[vecBranch])
        colNames.extend(tempColNames)
        if len(tempColNames) == 0:
            #print "removing", vecBranch
            branches.remove(vecBranch)

    colNames = sorted(list(set(colNames)))
    colTypeDict = {}
    for colName in colNames:
        baseName = re.split('_\d',colName)[0]
        baseType = branchBaseTypes[baseName]
        colTypeDict[colName]=baseType

    structDF = pd.DataFrame(columns=colNames)
    print 'Converting and dumping', tree.GetName()

    evtCount = 0
    for df in read_root(inFName, tree.GetName(), chunksize=chunkSize, columns=branches):
        flatten(df)
        df.fillna(value=-999, inplace=True)

        for col in df.keys():
            if col in colTypeDict:
                df[col] = df[col].astype(colTypeDict[col])
            else:
                print "can't find", col

        # Now need to make sure that the structure stays the same
        outDF = structDF.append([df])
        # Replace NaN with -999 so we can use ints
        outDF.fillna(value=-999, inplace=True)

        # Make sure everything has the correct type in the new full dataframe 
        for col in outDF.keys():
            if col in colTypeDict:
                outDF[col] = outDF[col].astype(colTypeDict[col])
            else:
                print "can't find", col

        evtCount += chunkSize
        procStr = 'processed %d/%d' % (evtCount, totalEvents)
        print procStr
        outDF.to_hdf(outFName, tree.GetName(), append=True)
        del df
    
def convertROOTFilesPandas(inFName, outPath='./', treeNames=[], chunkSize=10000, mergeTreeNames=[], branchVetoList=[]):
    """! 
    Convert trees in root file to pandas dataframes. These dataframes are not flattened (you can have columns that consist
    of numpy arrays) and are stored as an HDF5 files. If no treeNames or branch names are specified all branches
    from all trees are converted. Each tree is its own dataframe.
    
    @param inFName: string with the path to the input ROOT file
    @param treeNames: list of tree names. If left empty all trees are converted. 
    """

    outFName=outPath+inFName.split('.root')[0].split('/')[-1]+'.h5'
    print "Writing to", outFName
    inF = TFile.Open(inFName);
    
    # if no trees are specified just get all branches of all trees
    if treeNames == []:
        for key in inF.GetListOfKeys():
            if not (key.GetClassName()=='TTree' or key.GetClassName()=='TChain'):
                continue;
            tree = inF.Get(key.GetName());
            convertTree(inFName, outFName, tree, chunkSize, branchVetoList=branchVetoList)                        
    else:
        for treeName in treeNames:
            tree = inF.Get(treeName);
            convertTree(inFName, outFName, tree, chunkSize, branchVetoList=branchVetoList)
              
    inF.Close();
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert ROOT file to HDF5 file.')
    parser.add_argument('inFName', type=str, help='Location of input file')
    parser.add_argument('--outPath', type=str, help='Path to put output files in.', default='./')
    parser.add_argument('--treeNames', type=str, help='List of the name of trees to convert.', default='')
    parser.add_argument('--mergeTreeNames', type=str, help='List of trees you want to merge into one DF.', default='')
    parser.add_argument('--branchVetoList', type=str, help='List of regex for veto of branches.', default='mc.*,VRTop.*,VRW.*,Prime.*,TruthType.*,AntiKt2.*,AntiKt3.*,.*Children.*,.*Photon.*,.*DRMin.*,track.*,ckkw.*,qsf.*,fac.*,.*77,.*85,.*60,.*70,renorm.*,.*Trigger.*,.*Tags,Top\dChi2.*,W\dChi2.*,TrkIso.*,ptcone.*,TruthJetIsBTagged.*')
    parser.add_argument('--chunkSize', type=int, help='Number of events to process at a time.', default=10000)
    args = parser.parse_args()

    treeNames = []
    if args.treeNames != '':
        treeNames = [item.lstrip().rstrip() for item in args.treeNames.split(',')]
    
    mergeTreeNames = []
    if args.mergeTreeNames != '':
        mergeTreeNames = [item.lstrip().rstrip() for item in args.mergeTreeNames.split(',')]
        
    branchVetoList = []
    if args.branchVetoList != '':
        branchVetoList = [item.lstrip().rstrip() for item in args.branchVetoList.split(',')]
        
    convertROOTFilesPandas(args.inFName, outPath=args.outPath, treeNames=treeNames, chunkSize=args.chunkSize,
                           mergeTreeNames=mergeTreeNames, branchVetoList=branchVetoList)
