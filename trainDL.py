#!/usr/bin/env python
"""@package docstring
This module preps the data and trains and tests various NNs
"""

import sys, os, argparse, matplotlib, cutStrings, json
import numpy as np
np.random.seed(19)

matplotlib.use('Agg')
import matplotlib.pyplot as plt

from keras.models import load_model
from keras import backend as K
K.set_image_dim_ordering('th')

from models import models
from MLUtils import *

# Branches used for training
branchSets = {
    "gFEX":["gTowerEt"],
    }

# Set of branches to also grab for comaprisons. The branch name in a tuple (if accessing a vector) or string comes first. Then the scaling, binning and units.
extraBranchInfo = {"gFEX":[(('AntiKt10LCTopoTrimmedSmallR20PtFrac5Jets_pt[0]',0,1), [i*20 for i in range(31)], 1./1000, 'GeV'),
                           (("AntiKt4LCTopoJets_pt[0]", 0, 1), [i*20 for i in range(31)], 1./1000, 'GeV'),
                           (("AntiKt4LCTopoJets_pt[3]", 0, 1), [i*5 for i in range(31)], 1./1000, 'GeV'),
                           (("AntiKt4EMTopoJets_pt[0]", 0, 1), [i*20 for i in range(31)], 1./1000, 'GeV'),
                           (("AntiKt4EMTopoJets_pt[3]", 0, 1), [i*5 for i in range(31)], 1./1000, 'GeV'),
                           (("AntiKt4TruthJets_pt[0]", 0, 1), [i*20 for i in range(31)], 1./1000, 'GeV'),
                           (("AntiKt4TruthJets_pt[3]", 0, 1), [i*5 for i in range(31)], 1./1000, 'GeV'),
                           (("AntiKt10TruthJets_pt[0]", 0, 1), [i*20 for i in range(31)], 1./1000, 'GeV'),
                           (("CalibratedAntiKt4EMTopoJets_pt[0]", 0, 1), [i*20 for i in range(31)], 1./1000, 'GeV'),
                           (("CalibratedAntiKt4EMTopoJets_pt[3]", 0, 1), [i*5 for i in range(31)], 1./1000, 'GeV'),
                           (("AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJets_pt[0]", 0, 1), [i*20 for i in range(31)], 1./1000, 'GeV'),
                           (("AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJets_m[0]", 0, 1), [i*20 for i in range(31)], 1./1000, 'GeV'),
                           
                           (("AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20_pt[0]", 0, 1), [i*20 for i in range(31)], 1./1000, 'GeV'),
                           (("AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20_m[0]", 0, 1), [i*20 for i in range(31)], 1./1000, 'GeV'),
                           ("NJetsAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20", [i for i in range(21)], 1, ''),
                           ("HtAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20", [i*50 for i in range(21)], 1./1000, 'GeV'),
                           ("MaxDeltaPhiAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20", [-4+i*0.4 for i in range(21)], 1, ''),
                           ("MaxDeltaEtaAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20", [i*0.2 for i in range(21)], 1, ''),
                           ("MaxDeltaRAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20", [i*0.2 for i in range(41)], 1, ''),
                           ("LeadSubleadDeltaPhiAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20", [-4+i*0.4 for i in range(21)], 1, ''),
                           ("LeadSubleadDeltaEtaAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20", [i*0.2 for i in range(21)], 1, ''),
                           ("LeadSubleadDeltaRAntiKt10LCTopoTrimmedSmallR20PtFrac5SubJetsPt20", [i*0.2 for i in range(41)], 1, ''),

                           (("TruthTopPt[0]", 0, 1), [i*20 for i in range(31)], 1, 'GeV'),
                           (("TruthTopPt[1]", 0, 1), [i*20 for i in range(31)], 1, 'GeV'),
                           (("TruthTopDecayType[0]", 0, 1), [-20+i for i in range(40)], 1, ''),
                           ("AntiKt4LCTopoJets_n", [i*10 for i in range(21)], 1, ''),
                           ("AntiKt4EMTopoJets_n", [i*10 for i in range(21)], 1, ''),
                           ("AntiKt4TruthJets_n", [i*10 for i in range(21)], 1, ''),
                           ("AntiKt10TruthJets_n", [i*10 for i in range(21)], 1, ''),
                           ("actualIntPerCross", [i*10 for i in range(21)], 1, ''),
                           ("CalibratedAntiKt4EMTopoJets_n", [i*10 for i in range(21)], 1, ''),

                           (("AntiKt4EMTopoPt20_pt[0]", 0, 1), [i*20 for i in range(31)], 1./1000, 'GeV'),
                           (("AntiKt4EMTopoPt20_pt[3]", 0, 1), [i*5 for i in range(31)], 1./1000, 'GeV'),
                           ("NJetsAntiKt4EMTopoPt20", [i for i in range(21)], 1, ''),
                           ("HtAntiKt4EMTopoPt20", [i*50 for i in range(21)], 1./1000, 'GeV'),
                           ("MaxDeltaPhiAntiKt4EMTopoPt20", [-4+i*0.4 for i in range(21)], 1, ''),
                           ("MaxDeltaEtaAntiKt4EMTopoPt20", [i*0.2 for i in range(21)], 1, ''),
                           ("MaxDeltaRAntiKt4EMTopoPt20", [i*0.2 for i in range(41)], 1, ''),
                           ("LeadSubleadDeltaPhiAntiKt4EMTopoPt20", [-4+i*0.4 for i in range(21)], 1, ''),
                           ("LeadSubleadDeltaEtaAntiKt4EMTopoPt20", [i*0.2 for i in range(21)], 1, ''),
                           ("LeadSubleadDeltaRAntiKt4EMTopoPt20", [i*0.2 for i in range(41)], 1, ''),
 
                           ("AntiKt10LCTopoTrimmedSmallR20PtFrac5Jets_n",[i*10 for i in range(21)], 1, ''),
                           ("AntiKt10LCTopoTrimmedSmallR20PtFrac5SubJets_n", [i*10 for i in range(21)], 1, ''),
                           ("eventNumber", [i*10 for i in range(200)], 1, ''),
                   ],
                 }

extraBranchSets = {}
for key in extraBranchInfo:
    extraBranchSets[key] = []
    for branchInfo in extraBranchInfo[key]:
        extraBranchSets[key].append(branchInfo[0])

availEtaBins = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.5, 2.7, 2.9, 3.1, 3.2, 3.5, 4.0, 4.45, 4.9]
phiGran = 0.2

def generator(xTrain, yTrain, sampleWeight, batchSize):
    """ This function rotates the image in phi steps to augment the data. This can cause a quick increase in data set size to the
    point where the dataset won't fit in memory anymore. An additional concern with using this is that the data has to be mixed because 
    overtraining can occure when simply looking at images that have a pixel (in phi) shift. Thus this function is not yet used."""
    nPhiPix = xTrain.shape[3]
    nEtaPix = xTrain.shape[2]
    zDepth = xTrain.shape[1]
    print nPhiPix, nEtaPix
    while True:
        grids = []
        newYs = []
        newSampWeights = []
        for batchI in range(batchSize):
            grid = xTrain[batchI]
            y = yTrain[batchI]
            weight = sampleWeight[batchI]
            for shift in range(5):
                phiShifted = np.roll(grid, shift, axis=2)
                grids.append(phiShifted)
                # Now let's swap positive and negative eta!
                grids.append(np.roll(phiShifted, nEtaPix/2, axis=1))
                newYs+=[y,y]
                newSampWeights+=[weight, weight]
        grids = np.array(grids)
        newYs = np.array(newYs)
        newSampWeights = np.array(newSampWeights)
        yield (grids, newYs, newSampWeights)    
                

def trainDL(modelNames, modelFPath, sigFName, bkgFNames, epochs=20, batchSize=32, nTrainSamp=10000, nTestSamp=10000, precutKey='NoCut', maxEta=2.8, branches='gFEX', extraBranchKey='gFEX', weightBranches=[], shortSigName='signal', shortBkgName='bkg', update=False):
    """ This functions trains a specified set of models"""
    # The granularity in the gFEX isn't entirely uniform in eta
    etaBins = [etaBin for etaBin in availEtaBins if etaBin <= maxEta]
    etaBins = [-1.*etaBins[-1*i] for i in range(1,len(etaBins))]+etaBins
    
    nEtaPix = len(etaBins)-1
    nPhiPix = int(round(6.4/phiGran))
    dPhi = (2*math.pi)/nPhiPix
    phiBins = [-1*math.pi+i*dPhi for i in range(nPhiPix+1)]

    maxEtaStr = "_%.2f" % maxEta
    
    if extraBranchKey in extraBranchSets:
        tempExtraBranches = extraBranchSets[extraBranchKey]
    else:
        tempExtraBranches = []
    (xTrain, yTrain, xTest, yTest, weightsTrain, weightsTest, extraVarsTrain, extraVarsTest, sigGrid, bkgGrid, sigExtraVars, bkgExtraVars, sigExtraVarsSubset, totalBkgExtraVarsSubset, sigWeights, bkgWeights) = prepROOTDataML2D(nTrainSamp, nTestSamp, branchSets[branches], sigFName, bkgFNames, etaBins, phiBins, precutKey=precutKey, suffix='gFEX'+maxEtaStr, weightBranches=[], sigTreeName="mytree", bkgTreeName="mytree", sigFriendName='mytreeNew', bkgFriendName='mytreeNew', extraBranches=tempExtraBranches, shortSigName=shortSigName, shortBkgName=shortBkgName, update=update)
    
    print "Training with gFEX model", modelNames
    for modelName in modelNames:
        print "shape of input", (nEtaPix,nPhiPix,len(branchSets[branches]))
        model = models[modelName]((len(branchSets[branches]),nEtaPix,nPhiPix), modelName, modelFPath)
        model.fit(xTrain, yTrain, epochs=epochs, batch_size=batchSize, shuffle=False,sample_weight=weightsTrain, validation_data=(xTest,yTest))
        # Store model to file
        model.save(modelFPath+modelName+'.h5')
        scores = model.evaluate(xTrain, yTrain,sample_weight=weightsTrain)
        scoresTest = model.evaluate(xTest, yTest,sample_weight=weightsTest)

        print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))
        print("\nTest sample, %s: %.2f%%" % (model.metrics_names[1], scoresTest[1]*100))
        model.summary()
    
def testModel(sigFName, bkgFNames, modelFiles, nTrainSamp=10000, nTestSamp=10000, precutKey='NoCut', weightStr="1", maxEta=2.8, branches='gFEX', extraBranchKey="gFEX", shortSigName='signal', shortBkgName='bkg', update=False, desiredRate=50):
    """ This function produces various figures of merits for the trained model such as training and testing output, trained weights,
    and turn-on curves."""
    
    # The granularity in the gFEX isn't entirely uniform in eta
    etaBins = [etaBin for etaBin in availEtaBins if etaBin <= maxEta]
    etaBins = [-1.*etaBins[-1*i] for i in range(1,len(etaBins))]+etaBins

    nEtaPix = len(etaBins)-1
    nPhiPix = int(round(6.4/phiGran))
    dPhi = (2*math.pi)/nPhiPix
    phiBins = [-1*math.pi+i*dPhi for i in range(nPhiPix+1)]

    maxEtaStr = "_%.2f" % maxEta
    testTrainInfo = prepROOTDataML2D(nTrainSamp, nTestSamp, branchSets[branches], sigFName, bkgFNames, etaBins, phiBins, precutKey=precutKey, suffix='gFEX'+maxEtaStr, weightBranches=[], sigTreeName="mytree", bkgTreeName="mytree", sigFriendName='mytreeNew', bkgFriendName='mytreeNew', extraBranches=extraBranchSets[extraBranchKey], shortSigName=shortSigName, shortBkgName=shortBkgName, update=update)

    testTrainData = testTrainInfo[:4]
    testTrainWeights = testTrainInfo[4:6]
    extraVarsTrain,extraVarsTest = testTrainInfo[6:8]
    allData = testTrainInfo[8:10]
    extraVars = testTrainInfo[10:12]
    bkgWeights = testTrainInfo[-1]
    models = {}
    sigBkgEffs= {}
    bkgNNCut = 0.2
    for modelFile in modelFiles:
        modelName = modelFile.split("/")[-1]
        print "Fetching", modelName
        model = load_model(modelFile+".h5")
        model.summary()
        model.save_weights(modelName+'_weights.h5')
        outfile = open(modelName+'.json','wb')
        jsonString = model.to_json()
        with outfile:
            obj = json.loads(jsonString)
            json.dump(obj, outfile, sort_keys=True,indent=4, separators=(',', ': '))
            outfile.write('\n')
        models[modelName] = model
        
        print "Getting signal and background efficiency for", modelName
        responseFName = 'response_'+modelName+'.pkl'

        (responses, sigBkgEffs[modelName]) = getSigBkgEffFromModel(model, testTrainData, allData, responseFName, binning=[i*.001 for i in range(1001)])
        testTrainResponses = responses[:4]
        allResponses = responses[4:]
        
        print "Plotting response for", modelName
        #plotResponse(testTrainResponses, suffix=modelName)
        
        # # print "Making rate plots."
        # Now making trigger performance plots
        (sigNNCut, effHists) = plotRate(allResponses, extraVars, extraBranchInfo[extraBranchKey], suffix='_'+modelName, desiredRate=desiredRate)

         #plotVars(allResponses, extraVars, extraBranchInfo[extraBranchKey], sigNNCut, suffix='_'+modelName)
        # # print "Plotting weights and making input images that maximimally activate a filter."
        plotWeights(model, 0, suffix='_'+modelName)
        #plotFinalWeights(model, suffix='_'+modelName)
        #deepDraw(model, nXBins=nEtaPix, nYBins=nPhiPix, suffix='_'+modelName)
        #findInputMaxMinAct(model, allData, extraVars, extraBranchInfo[extraBranchKey], 0, nImages=5)
        #plotGTowerQuant(allData, allResponses, sigNNCut)
        applyGTowerCut(allData, extraVars, extraBranchInfo[extraBranchKey], sigNNCut, desiredRate=desiredRate, nnEffHists=effHists)
        makePseudoData(model, nXBins=nEtaPix, nYBins=nPhiPix)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train a CNN for calo trigger towers')
    parser.add_argument('--sigFName', type=str, help='Location of signal file', default='/projects/chep/whopkins/gFEXSamps/user.whopkins.206286.MadGraphPythia8_AU2CTEQ6L1_gg_hh125_4b_lambda01.recon.AOD.e3202_s3142_s3143_r9589.noSCProv.v31_2_OUTPUT/*')
    parser.add_argument('--bkgFNames', type=str, help='Location of background files', default='projects/chep/whopkins/gFEXSamps/user.whopkins.147910.Pythia8_AU2CT10_jetjet_JZ0W.recon.AOD.e2403_s3142_s3143_r9589.noSCProv.v31_OUTPUT/*')

    parser.add_argument('--sigTreeName', type=str, help='Signal tree name', default='mytree')
    parser.add_argument('--bkgTreeName', type=str, help='Background tree name', default='mytree')
    
    parser.add_argument('--test', action='store_true', help="Don't train, test")
    parser.add_argument('--update', action='store_true', help="Update h5 files by redoing ROOT conversion.")
    parser.add_argument('--branches', type=str, default="gFEX", help='Key to use for branch dictionary')
    parser.add_argument('--extraBranchKey', type=str, default="gFEX", help='Key to use for extra branches to be used (not for training but for studies)')

    parser.add_argument('--shortSigName', type=str, default='hh4b', help='Short name used to describe signal h5 file.')
    parser.add_argument('--shortBkgName', type=str, default='jz0w', help='Short name used to describe background h5 file.')

    parser.add_argument('--modelNames', type=str, help='Name of models, comma delimited.')
    parser.add_argument('--modelFPath', type=str, default='/projects/chep/whopkins/caloTowerFiles/', help='Path where model should be saved.')
    parser.add_argument('--nTrainSamp', type=int, help='Number of events to use for training', default=25000)
    parser.add_argument('--nTestSamp', type=int, help='Number of events to use for testing', default=5000)
    parser.add_argument('--precutKey', type=str, help='Preselection cut to use', default='OnlyHadTopBCIDNoHighPtJet')
    parser.add_argument('--maxEta', type=float, help='Maximum eta', default=3.15)
    parser.add_argument('--dEta', type=float, help='Delta eta', default=0.2)
    parser.add_argument('--dPhi', type=float, help='Delta phi', default=0.2)
    parser.add_argument('--epochs', type=int, help='Number of epochs', default=20)
    parser.add_argument('--desiredRate', type=int, help='Desired rate in KHz', default=50)
    parser.add_argument('--suffix', type=str, help='suffix to identify different grid files and model files', default="")

    args = parser.parse_args() 
    bkgFNames = [bkgFName for bkgFName in args.bkgFNames.split(',')]
   
    if args.test:
        testModel(args.sigFName, bkgFNames, [args.modelFPath+"/"+modelName for modelName in args.modelNames.split(",")], nTrainSamp=args.nTrainSamp, nTestSamp=args.nTestSamp, precutKey=args.precutKey, maxEta=args.maxEta, shortSigName=args.shortSigName, shortBkgName=args.shortBkgName, update=args.update, desiredRate=args.desiredRate);
    else:
        modelNames = args.modelNames.split(",")   
        trainDL(modelNames, args.modelFPath, args.sigFName, bkgFNames, epochs=args.epochs, nTrainSamp=args.nTrainSamp, nTestSamp=args.nTestSamp, maxEta=args.maxEta, branches=args.branches, extraBranchKey=args.extraBranchKey, precutKey=args.precutKey, shortSigName=args.shortSigName, shortBkgName=args.shortBkgName, update=args.update);
