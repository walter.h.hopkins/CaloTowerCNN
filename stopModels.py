#!/usr/bin/env python
from keras.models import Sequential, load_model, Model
from keras.layers.core import Dense, Activation, Dropout, Flatten
from keras.layers.normalization import BatchNormalization
from keras.layers import Conv2D, MaxPooling2D, Input
from keras.layers.merge import Concatenate
from keras import initializers

def modelConv2D_3x3_2F_1L_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(2, (3,3), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_3x3_4F_1L_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(4, (3,3), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_5x5_8F_1L_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(8, (5,5), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_5x5_16F_1L_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (5,5), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_7x7_8F_1L_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(8, (7,7), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_7x7_16F_1L_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (7,7), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_9x9_8F_1L_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(8, (9,9), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_9x9_16F_1L_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (9,9), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_3x3_2F_1L_fc32_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(2, (3,3), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_3x3_4F_1L_fc32_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(4, (3,3), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_5x5_8F_1L_fc32_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(8, (5,5), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_5x5_16F_1L_fc32_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (5,5), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_7x7_8F_1L_fc32_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(8, (7,7), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_7x7_16F_1L_fc32_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (7,7), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_9x9_8F_1L_fc32_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(8, (9,9), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_9x9_16F_1L_fc32_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (9,9), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_3x3_2F_1L_fc16_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(2, (3,3), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_3x3_4F_1L_fc16_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(4, (3,3), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_5x5_8F_1L_fc16_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(8, (5,5), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_5x5_16F_1L_fc16_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (5,5), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_7x7_8F_1L_fc16_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(8, (7,7), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_7x7_16F_1L_fc16_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (7,7), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_9x9_8F_1L_fc16_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(8, (9,9), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_9x9_16F_1L_fc16_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (9,9), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_9x9_16F_1L_fc8_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (9,9), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(8, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_9x9_16F_1L_fc8_noPadding_stride2x2(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (9,9), activation='relu', input_shape=inputShape,strides=(2,2)))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(8, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_9x9_8F_1L_fc8_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(8, (9,9), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(8, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_9x9_32F_1L_fc4_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(32, (9,9), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(4, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_9x9_16F_1L_fc4_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (9,9), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(4, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model


def modelConv2D_9x9_8F_1L_fc4_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(8, (9,9), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(4, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model




def modelConv2D_11x11_8F_1L_fc16_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(8, (11,11), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_11x11_16F_1L_fc16_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (11,11), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_11x11_16F_1L_fc32_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (11,11), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_11x11_16F_1L_fc32_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (11,11), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_11x11_8F_1L_fc32_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(8, (11,11), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_11x11_16F_1L_fc8_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(16, (11,11), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(8, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model

def modelConv2D_11x11_8F_1L_fc8_noPadding(inputShape, modelName, modelFPath):
    model = Sequential()
    
    model.add(Conv2D(8, (11,11), activation='relu', input_shape=inputShape))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    
    model.add(Flatten())
    model.add(Dense(8, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='Adam', metrics=['accuracy',])
    # Store model to file
    model.save(modelFPath+modelName+'.h5')
    return model



models = {
    "modelConv2D_3x3_2F_1L_noPad":modelConv2D_3x3_2F_1L_noPadding,
    "modelConv2D_3x3_4F_1L_noPad":modelConv2D_3x3_4F_1L_noPadding,

    "modelConv2D_5x5_8F_1L_noPad":modelConv2D_5x5_8F_1L_noPadding,
    "modelConv2D_5x5_16F_1L_noPad":modelConv2D_5x5_16F_1L_noPadding,
    
    "modelConv2D_7x7_8F_1L_noPad":modelConv2D_7x7_8F_1L_noPadding,
    "modelConv2D_7x7_16F_1L_noPad":modelConv2D_7x7_16F_1L_noPadding,
    
    "modelConv2D_9x9_8F_1L_noPad":modelConv2D_9x9_8F_1L_noPadding,
    "modelConv2D_9x9_16F_1L_noPad":modelConv2D_9x9_16F_1L_noPadding,

    
    "modelConv2D_3x3_2F_1L_fc32_noPad":modelConv2D_3x3_2F_1L_fc32_noPadding,
    "modelConv2D_3x3_4F_1L_fc32_noPad":modelConv2D_3x3_4F_1L_fc32_noPadding,

    "modelConv2D_5x5_8F_1L_fc32_noPad":modelConv2D_5x5_8F_1L_fc32_noPadding,
    "modelConv2D_5x5_16F_1L_fc32_noPad":modelConv2D_5x5_16F_1L_fc32_noPadding,
    
    "modelConv2D_7x7_8F_1L_fc32_noPad":modelConv2D_7x7_8F_1L_fc32_noPadding,
    "modelConv2D_7x7_16F_1L_fc32_noPad":modelConv2D_7x7_16F_1L_fc32_noPadding,
    
    "modelConv2D_9x9_8F_1L_fc32_noPad":modelConv2D_9x9_8F_1L_fc32_noPadding,
    "modelConv2D_9x9_16F_1L_fc32_noPad":modelConv2D_9x9_16F_1L_fc32_noPadding,

    
    "modelConv2D_3x3_2F_1L_fc16_noPad":modelConv2D_3x3_2F_1L_fc16_noPadding,
    "modelConv2D_3x3_4F_1L_fc16_noPad":modelConv2D_3x3_4F_1L_fc16_noPadding,

    "modelConv2D_5x5_8F_1L_fc16_noPad":modelConv2D_5x5_8F_1L_fc16_noPadding,
    "modelConv2D_5x5_16F_1L_fc16_noPad":modelConv2D_5x5_16F_1L_fc16_noPadding,
    
    "modelConv2D_7x7_8F_1L_fc16_noPad":modelConv2D_7x7_8F_1L_fc16_noPadding,
    "modelConv2D_7x7_16F_1L_fc16_noPad":modelConv2D_7x7_16F_1L_fc16_noPadding,
    
    "modelConv2D_9x9_8F_1L_fc16_noPad":modelConv2D_9x9_8F_1L_fc16_noPadding,
    "modelConv2D_9x9_16F_1L_fc16_noPad":modelConv2D_9x9_16F_1L_fc16_noPadding,
    
    "modelConv2D_9x9_8F_1L_fc8_noPad":modelConv2D_9x9_8F_1L_fc8_noPadding,
    "modelConv2D_9x9_16F_1L_fc8_noPad":modelConv2D_9x9_16F_1L_fc8_noPadding,
    "modelConv2D_9x9_16F_1L_fc8_noPad_stride2x2":modelConv2D_9x9_16F_1L_fc8_noPadding_stride2x2,

    "modelConv2D_9x9_8F_1L_fc4_noPad":modelConv2D_9x9_8F_1L_fc4_noPadding,
    "modelConv2D_9x9_16F_1L_fc4_noPad":modelConv2D_9x9_16F_1L_fc4_noPadding,
    "modelConv2D_9x9_32F_1L_fc4_noPad":modelConv2D_9x9_32F_1L_fc4_noPadding,

    
    "modelConv2D_11x11_8F_1L_fc32_noPad":modelConv2D_11x11_8F_1L_fc32_noPadding,
    "modelConv2D_11x11_16F_1L_fc32_noPad":modelConv2D_11x11_16F_1L_fc32_noPadding,

    "modelConv2D_11x11_8F_1L_fc16_noPad":modelConv2D_11x11_8F_1L_fc16_noPadding,
    "modelConv2D_11x11_16F_1L_fc16_noPad":modelConv2D_11x11_16F_1L_fc16_noPadding,
    
    "modelConv2D_11x11_8F_1L_fc8_noPad":modelConv2D_11x11_8F_1L_fc8_noPadding,
    "modelConv2D_11x11_16F_1L_fc8_noPad":modelConv2D_11x11_16F_1L_fc8_noPadding,
    
}
