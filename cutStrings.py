import sys
name_CA_pTbV1 = "CA_pTbV1"
name_CA_pTjV4 = "CA_pTjV4"
if 'isMunich' in sys.argv:
    name_CA_pTbV1 = "CA_pTbV[0]"
    name_CA_pTjV4 = "CA_pTjV[3]"

precutSR = ("("
            " NJets>=4"
            " && MetTrack >30"
            " && NBJets>=1"
            " && Met>250"
            " && DPhiMetTrackMet < 1/3.*TMath::Pi()"
            " && JetDPhiMetMin2 > 0.4"
            " && JetPt[3] > 40 "
            " && PassMetTrigger"
          ")"
)

precut1LepCR = ("("
                " NJets>=4"
                " && JetDPhiMetMin2 > 0.4"
                " && NBJets>=1"
                " && Met>250"
                " && MtMetLep < 100"
                " && PassMetTrigger"
                ")"
)

precutSRNonISR = ("("
            " NJets>=4"
            " && MetTrack >30"
            " && NBJets>=1"
            " && Met>250"
            " && DPhiMetTrackMet < 1/3.*TMath::Pi()"
            " && JetDPhiMetMin3 > 0.4"
            " && JetPt[3] > 40 "
            " && PassMetTrigger"
                  " && NAntiKt12>=2"
          ")"
)

precut1LepCRNonISR = ("("
                " NJets>=4"
                " && JetDPhiMetMin3 > 0.4"
                " && NBJets>=1"
                " && Met>250"
                " && MtMetLep < 100"
                " && PassMetTrigger"
                ")"
)

precut2LepCR = ("("
                "1"
                "&& LLMass > 86 && LLMass < 96"
                "&& OSLep"
                "&& MetLep > 70"
                "&& Met < 50"
                "&& JetPt[1] > 40"
                "&& ( (NSigElectrons==2 && ElPt[0] > 28 && ElPt[1] > 28) || (NSigMuons==2 && MuPt[0] > 28 && MuPt[1] > 28))"
                ")"
)


srCuts = {    
    "SRA_TT":"( ("+precutSRNonISR+") && (AntiKt12M[0]>120 && AntiKt12M[1] > 120 && Met > 400 && AntiKt8M[0]>60 && MtBMin > 200 && MtTauCand < 0 && DRBB > 1 && MT2Chi2>400 && NBJets>=2 ))",

    "SRA_TW":"( ("+precutSRNonISR+") && (AntiKt12M[0]>120 && AntiKt12M[1] < 120 && AntiKt12M[1] > 60 && Met > 500 && AntiKt8M[0]>60 && MtBMin > 200 && MtTauCand < 0 && MT2Chi2>400 && NBJets>=2))",
   
    "SRA_T0":"( ("+precutSRNonISR+") && (AntiKt12M[0]>120 && AntiKt12M[1] < 60 &&  AntiKt8M[0] > 60 && Met>550 && MtBMin > 200 && MtTauCand < 0 && MT2Chi2>500 && NBJets>=2 ))",
  
    "SRB_TT":"( ("+precutSRNonISR+") && (AntiKt12M[0]>120 && AntiKt12M[1] > 120 && DRBB > 1.2 && MtBMax > 200 && MtBMin > 200 && MtTauCand < 0 && NBJets>=2))",

    "SRB_TW":"( ("+precutSRNonISR+") && (AntiKt12M[0]>120 && AntiKt12M[1] < 120 && AntiKt12M[1] > 60 && DRBB > 1.2 && MtBMax > 200 && MtBMin > 200 && MtTauCand < 0 && NBJets>=2))",

    "SRB_T0":"( ("+precutSRNonISR+") && (AntiKt12M[0]>120 && AntiKt12M[1] < 60 &&  MtBMin > 200 && DRBB > 1.2 && MtBMax > 200 && Met > 250 && MtTauCand < 0 && NBJets>=2))",     
    
    "SRC1":"( ("+precutSR+") && (CA_NbV >= 1 && CA_NjV >= 5 && "+name_CA_pTbV1+" > 40 && CA_MS > 300 && CA_dphiISRI > 3.00 && CA_PTISR > 400 && "+name_CA_pTjV4+" > 50 && CA_RISR >= 0.30 && CA_RISR <= 0.4))",
    "SRC2":"( ("+precutSR+") && (CA_NbV >= 1 && CA_NjV >= 5 && "+name_CA_pTbV1+" > 40 && CA_MS > 300 && CA_dphiISRI > 3.00 && CA_PTISR > 400 && "+name_CA_pTjV4+" > 50 && CA_RISR >= 0.4 && CA_RISR <= 0.5))",
    "SRC3":"( ("+precutSR+") && (CA_NbV >= 1 && CA_NjV >= 5 && "+name_CA_pTbV1+" > 40 && CA_MS > 300 && CA_dphiISRI > 3.00 && CA_PTISR > 400 && "+name_CA_pTjV4+" > 50 && CA_RISR >= 0.5 && CA_RISR <= 0.6))",
    "SRC4":"( ("+precutSR+") && (CA_NbV >= 1 && CA_NjV >= 5 && "+name_CA_pTbV1+" > 40 && CA_MS > 300 && CA_dphiISRI > 3.00 && CA_PTISR > 400 && "+name_CA_pTjV4+" > 50 && CA_RISR >= 0.6 && CA_RISR <= 0.7))",
    "SRC5":"( ("+precutSR+") && (CA_NbV >= 1 && CA_NjV >= 5 && "+name_CA_pTbV1+" > 40 && CA_MS > 300 && CA_dphiISRI > 3.00 && CA_PTISR > 400 && "+name_CA_pTjV4+" > 50 && CA_RISR >= 0.7 && CA_RISR <= 0.8))",
    
    "SRD_low":"( ("+precutSRNonISR+") && (NJets >= 5 && NBJets>=2 && Met > 250 && MtBMin > 250 && MtBMax > 300 && DRBB > 0.8 && JetPt[1] > 150 && JetPt[3]>100 && JetPt[4]>60 && JetPt[JetLeadTagIndex]+JetPt[JetSubleadTagIndex]>300 && MtTauCand < 0))",

    "SRD_high":"( ("+precutSRNonISR+") && (NJets >= 5 && JetPt[1]>150 && JetPt[3]>80 && JetPt[4]>60 && MtBMin>350 && MtBMax>450 && NBJets>=2 && Met > 250 && DRBB > 0.8 && JetPt[JetLeadTagIndex]+JetPt[JetSubleadTagIndex]>400 && MtTauCand < 0 ))",
    
    "SRE":"( ("+precutSRNonISR+") && ( Met > 550 && AntiKt8M[0] > 120 && AntiKt8M[1] > 80 && Ht > 800 && HtSig > 18 && MtBMin > 200 && NBJets>=2))",

    ## The following are pseudo SRs for final plots
    "SRC1_5":"( ("+precutSR+") && (CA_NbV >= 1 && NBJets >=1 && CA_NjV >= 5 && "+name_CA_pTbV1+" > 40 && CA_MS > 300 && CA_dphiISRI > 3.00 && CA_PTISR > 400 && "+name_CA_pTjV4+" > 50 && CA_RISR >= 0.3 && CA_RISR <= 0.8))",
}
srCuts["SRA"] = "("+srCuts["SRA_TT"]+"||"+srCuts["SRA_TW"]+"||"+srCuts["SRA_T0"]+")"
srCuts["SRB"] = "("+srCuts["SRB_TT"]+"||"+srCuts["SRB_TW"]+"||"+srCuts["SRB_T0"]+")"

cr1LepCuts = {
    "CRTopATT":"( ("+precut1LepCRNonISR+") && (DRBB > 1 && Met > 250 && AntiKt8M[0]>60 && MtMetLep > 30 && AntiKt12M[0] > 120 &&  AntiKt12M[1] > 120 && MtBMin > 100 && JetPt[1] > 80 && JetPt[3] > 40 && NBJets>=2 && MinDRBLep<1.5))",

    "CRTopATW":"( ("+precut1LepCRNonISR+") && (Met > 300 && AntiKt8M[0]>60 && MtMetLep > 30 && AntiKt12M[0] > 120 &&  AntiKt12M[1] < 120 && AntiKt12M[1] > 60 && MtBMin > 100 && JetPt[1] > 80 && JetPt[3] > 40 && NBJets>=2 && MinDRBLep<1.5))",
    
    "CRTopAT0":"( ("+precut1LepCRNonISR+") && (Met > 350 && AntiKt8M[0]>60 && MtMetLep > 30 && AntiKt12M[0] > 120 &&  AntiKt12M[1] < 60 && MtBMin > 100 && JetPt[1] > 80 && JetPt[3] > 40 && NBJets>=2 && MinDRBLep<1.5))",

    "CRTopBTT":"( ("+precut1LepCRNonISR+") && (DRBB > 1.2 && MtMetLep > 30 &&  AntiKt12M[0] > 120 &&  AntiKt12M[1] > 120 && MtBMin > 100 && MtBMax>200 && JetPt[1] > 80 && JetPt[3] > 40 && NBJets>=2 && MinDRBLep<1.5))",

    "CRTopBTW":"( ("+precut1LepCRNonISR+") && (DRBB > 1.2 && MtMetLep > 30 && AntiKt12M[0] > 120 &&  AntiKt12M[1] < 120 && AntiKt12M[1] > 60 && MtBMin > 100 && MtBMax>200 && JetPt[1] > 80 && JetPt[3] > 40 && NBJets>=2 && MinDRBLep<1.5))",

    "CRTopBT0":"( ("+precut1LepCRNonISR+") && (DRBB > 1.2 && MtMetLep > 30 && AntiKt12M[0] > 120 &&  AntiKt12M[1] < 60 && MtBMin > 100 && MtBMax>200 && JetPt[1] > 80 && JetPt[3] > 40 && NBJets>=2 && MinDRBLep<1.5))",
    
    "CRTopC":"( ("+precut1LepCR+") && (MtMetLep < 80 && JetPt[1] > 80 && JetPt[3] > 40 && NBJets>= 1 && (MinDRBLep < 2.) && CA_NjV >= 5 && CA_NbV >= 1 && CA_PTISR > 400 && "+name_CA_pTjV4+" > 40))",
    
    "CRTopD":"( ("+precut1LepCRNonISR+") && ( NJets>=5 && MtMetLep > 30 && MinDRBLep<1.5 && NBJets>=2 && Met > 250 && MtBMin > 100 && MtBMax > 100 && DRBB > 0.8 && JetPt[1] > 150 && JetPt[3]>80 && JetPt[JetLeadTagIndex]+JetPt[JetSubleadTagIndex]>300))",
    
    "CRTopE": "( ("+precut1LepCRNonISR+") && (MtMetLep > 30 && AntiKt8M[0] > 120 && AntiKt8M[1] > 80 && Ht > 500 && MtBMin > 100 && NBJets>=2 && MinDRBLep<1.5))",

    "CRST":"( ("+precut1LepCR+") && (MtMetLep > 30 && MtMetLep < 100 && AntiKt12M[0] > 120 && MtBMin > 200 && NBJets>=2 && DRBB>1.5 && MinDRBLep > 2 && JetPt[1]>80 && JetPt[3] > 40))",
    
    "CRW":"( ("+precut1LepCR+") && (MtMetLep > 30 && MtMetLep < 100 && AntiKt12M[0] < 60 && MinDRBLep > 2. && NBJets==1 && JetPt[1] > 80 && JetPt[3] > 40))",    
}

cr2LepCuts = {
    "CRZ_ICHEP":"( ("+precut2LepCR+") && (NBJets>=2 && NJets>=4) )", 
    
    "CRZAB_TT_TW":"( ("+precut2LepCR+") && (NBJets>=2 && NJets>=4) && MetLep > 100 && JetPt[1] > 80 && JetPt[3] > 40 && AntiKt12M[0] > 120 && AntiKt12M[1]>60)", 
    "CRZAB_T0":"( ("+precut2LepCR+") && (NBJets>=2 && NJets>=4) && MetLep > 100 && JetPt[1] > 80 && JetPt[3] > 40 && AntiKt12M[0] > 120 && AntiKt12M[1]<60)", 
    "CRZD":"( ("+precut2LepCR+") && (NBJets>=2 && NJets>=5) && MetLep > 100 && JetPt[1] > 80 && JetPt[3] > 40 && MtBMaxLep > 200 && MtBMinLep > 200)", 
    "CRZE":"( ("+precut2LepCR+") && (NBJets>=2 && NJets>=4) && MetLep > 100 && JetPt[1] > 80 && JetPt[3] > 40 && MtBMinLep > 200 && Ht>500)", 
}

cr1PhoCuts = {"CRTTGamma":("(NSigElectrons+NSigMuons==1 && NElectrons+NMuons==1 && NJets>=4 && NBJets>=2 && SigPhotonPt[0] > 150 &&"
                           "( ( (dsid==410000||dsid==407012) && HasMEPhoton==0) || (dsid!=410000 && dsid!=407012 && HasMEPhoton==1) ) "
                           "&& JetPt[1]>80 && JetPt[3]>40 "
                           #"&& MtMetLep>30"
                           #" && PhotonTrigMatch"
                           #"&& PassPhotonTrigger "
                           "&& PassLepTrigger && LepTrigMatch"
                           "&& ((NSigElectrons==1 && ElPt[0] > 28)||(NSigMuons==1 && MuPt[0] > 28))"
                           ")"
), 
}

vrCuts = {
    
    "VRTopATT":"( ("+precutSRNonISR+") && (AntiKt12M[0] > 120 &&  AntiKt12M[1] > 120 && AntiKt8M[0]>60 && MtBMin < 200 && MtBMin > 100 && JetPt[1] > 80 && JetPt[3] > 40 && NBJets>=2 && Met>300 && DRBB > 1))",

    "VRTopATW":"( ("+precutSRNonISR+") && (AntiKt12M[0] > 120 &&  AntiKt12M[1] > 60 && AntiKt12M[1] < 120 && AntiKt8M[0]>60 && MtBMin > 100 && MtBMin < 200 && JetPt[1] > 80 && JetPt[3] > 40 && NBJets>=2 && Met>400))",
          
    "VRTopAT0":"( ("+precutSRNonISR+") && (AntiKt12M[0] > 120 &&  AntiKt12M[1] < 60 && AntiKt8M[0]>60 && MtBMin > 100 && MtBMin < 200 && JetPt[1] > 80 && JetPt[3] > 40 && NBJets>=2 && Met>450))",
    
    "VRTopBTT":"( ("+precutSRNonISR+") && (AntiKt12M[0] > 120 &&  AntiKt12M[1] > 120 && DRBB > 1.2 && MtBMax > 200 && MtBMin > 100 && MtBMin < 200 && JetPt[1] > 80 && JetPt[3] > 40 && NBJets>=2 && Met>250))",
          
    "VRTopBTW":"( ("+precutSRNonISR+") && (AntiKt12M[0] > 120 &&  AntiKt12M[1] > 60 && AntiKt12M[1] < 120 && DRBB > 1.2 && MtBMax > 200 && MtBMin > 140 && MtBMin < 200 && JetPt[1] > 80 && JetPt[3] > 40 && NBJets>=2 && Met>250))",
          
    "VRTopBT0":"( ("+precutSRNonISR+") && (AntiKt12M[0] > 120 &&  AntiKt12M[1] < 60 && DRBB > 1.2 && MtBMax > 200 && MtBMin > 160 && MtBMin < 200 && JetPt[1] > 80 && JetPt[3] > 40 && NBJets>=2 && Met>250))",

    "VRTopC":"( ("+precutSR+") && ( CA_PTISR>400 && CA_NbV>=1 && CA_NjV>=4 && CA_MS>100 && "+name_CA_pTbV1+" > 40 && CA_MV/CA_MS < 0.6 && CA_dphiISRI < 3. && "+name_CA_pTjV4+" > 40))",
    
    "VRTopD":"( ("+precutSRNonISR+") && ( NJets>=5 && NBJets>=2 && Met > 250 && MtBMin < 200 && MtBMin > 100 && MtBMax > 300 && DRBB > 0.8 && JetPt[1] > 150 && JetPt[3]>80 && JetPt[JetLeadTagIndex]+JetPt[JetSubleadTagIndex]>300))",

    "VRTopE":"( ("+precutSRNonISR+") && (AntiKt8M[0] > 120 && AntiKt8M[1] > 80 && MtBMin > 100 && MtBMin < 200 && JetPt[1] > 80 && JetPt[3] > 40 && NBJets>=2 && Met>250))",
  
    "VRW":"( ("+precut1LepCR+") && (MtBMin > 150 && MtMetLep > 30 && MtMetLep < 100 && AntiKt12M[0] < 70 && MinDRBLep > 1.8 && NBJets>=2 && JetPt[1] > 80 && JetPt[3] > 40))", #goes with 1-lepton preselection
   
    # "VRZA_2b":"( ("+precutSRNonISR+") && (NBJets>=2 && NJets>=4 && JetPt[1] > 80 && MtBMin > 100 && AntiKt12M[0] < 120 && DRBB<1))",
    # "VRZA_ge1b":"( ("+precutSRNonISR+") && (NBJets>=1 && NJets>=4 && JetPt[1] > 80 && MtBMin > 150 && AntiKt12M[0] < 120 && DRBB<1))",
    # "VRZA_1b":"( ("+precutSRNonISR+") && (NBJets==1 && NJets>=4 && JetPt[1] > 80 && MtBMin > 200 && AntiKt12M[0] < 120 && DRBB<1))",
    # "VRZA_0b":"( ("+precutSRNonISR.replace("NBJets>=1", "NBJets==0")+") && (NJets>=4 && JetPt[1] > 80 && AntiKt12M[0] > 120 ))",
    
    "VRZAB":"(("+precutSRNonISR+") && (NBJets>=2 && JetPt[1] > 80 && NJets>=4 && MtBMin > 200 && MtTauCand < 0 && AntiKt12M[0] < 120 && DRBB<1))",
    "VRZD" :"(("+precutSRNonISR+") && (NBJets>=2 && JetPt[1] > 150 && NJets>=5 && MtBMin > 200 && MtTauCand < 0 && MtBMax>200 && DRBB<0.8))",
    #"VRZE" :"(("+precutSRNonISR+") && (NBJets>=2 && JetPt[1] > 80 && NJets>=4 && MtBMin > 200 && Ht > 500 && DRBB<1.0 && AntiKt8M[0] < 60))",
    "VRZE" :"(("+precutSRNonISR+") && (NBJets>=2 && JetPt[1] > 80 && NJets>=4 && MtBMin > 200 && HtSig > 14 && Ht > 500 && AntiKt8M[0] < 120 && DRBB<1.0))",
}

allCuts = srCuts.copy();
allCuts.update(cr1LepCuts);
allCuts.update(cr2LepCuts);
allCuts.update(cr1PhoCuts);
allCuts.update(vrCuts);
allCuts["precut1LepCR"] = precut1LepCR;
allCuts["precutSR"] = precutSR# precutSRNonISR;
allCuts["precutSRPlot"] = "("+precutSRNonISR+" && "+"MtBMin>50)";
allCuts["precutSRISR"] = "("+precutSR+" && "+"CA_RISR>0.25 && CA_NjV>=5)";
allCuts["NoCut"] = "(1)"
allCuts["OnlyHadTop"] = "(abs(TruthTopDecayType[0])!=11 && abs(TruthTopDecayType[1])!=11 && abs(TruthTopDecayType[0])!=13 && abs(TruthTopDecayType[1])!=13 && abs(TruthTopDecayType[0])!=15 && abs(TruthTopDecayType[1])!=15)"

allCuts["OnlyHadTopBCID"] = "(abs(TruthTopDecayType[0])!=11 && abs(TruthTopDecayType[1])!=11 && abs(TruthTopDecayType[0])!=13 && abs(TruthTopDecayType[1])!=13 && abs(TruthTopDecayType[0])!=15 && abs(TruthTopDecayType[1])!=15 && distFrontBunchTrain>20)"

allCuts["OnlyHadTopBCIDNoHighPtJet"] = "(abs(TruthTopDecayType[0])!=11 && abs(TruthTopDecayType[1])!=11 && abs(TruthTopDecayType[0])!=13 && abs(TruthTopDecayType[1])!=13 && abs(TruthTopDecayType[0])!=15 && abs(TruthTopDecayType[1])!=15 && distFrontBunchTrain>20 && CalibratedAntiKt4EMTopoJets_pt[0]/1000.<400)"

allCuts["OnlyHadTopBCIDNoHighPtJetNoHighPtTowers"] = "(abs(TruthTopDecayType[0])!=11 && abs(TruthTopDecayType[1])!=11 && abs(TruthTopDecayType[0])!=13 && abs(TruthTopDecayType[1])!=13 && abs(TruthTopDecayType[0])!=15 && abs(TruthTopDecayType[1])!=15 && distFrontBunchTrain>20 && CalibratedAntiKt4EMTopoJets_pt[0]/1000.<400 && Max$(gTowerEt)< 150000)"

allCuts["OnlyHadTopBCIDNoHighPtJetNoHighPtTowers100"] = "(abs(TruthTopDecayType[0])!=11 && abs(TruthTopDecayType[1])!=11 && abs(TruthTopDecayType[0])!=13 && abs(TruthTopDecayType[1])!=13 && abs(TruthTopDecayType[0])!=15 && abs(TruthTopDecayType[1])!=15 && distFrontBunchTrain>20 && CalibratedAntiKt4EMTopoJets_pt[0]/1000.<400 && Max$(gTowerEt)< 100000)"

allCuts["OnlyHadTopLowTopPt"] = "(abs(TruthTopDecayType[0])!=11 && abs(TruthTopDecayType[1])!=11 && abs(TruthTopDecayType[0])!=13 && abs(TruthTopDecayType[1])!=13 && abs(TruthTopDecayType[0])!=15 && abs(TruthTopDecayType[1])!=15 && TruthTopPt[0]<300)"

allCuts["OnlyHadTopBCIDCentralTop"] = "(abs(TruthTopDecayType[0])!=11 && abs(TruthTopDecayType[1])!=11 && abs(TruthTopDecayType[0])!=13 && abs(TruthTopDecayType[1])!=13 && abs(TruthTopDecayType[0])!=15 && abs(TruthTopDecayType[1])!=15 && distFrontBunchTrain>20 && (fabs(TruthTopEta[0])<1.4 || TruthTopEta[0]<-999))"

allCuts["HighZPt"] = "(dsid==364155 || dsid==364154)"


weightStr = "(ThreeBodyWeightNum*JVTSF*sherpaNJetsWeight*BTagSF*PileupWeight*FinalXSecWeight*GenWeight * 1000)";
lepWeightStr = "(JVTSF*sherpaNJetsWeight*ElecSF*MuonSF*BTagSF*PileupWeight*FinalXSecWeight*GenWeight * 1000)";
photonWeightStr = "(PhotonSF*JVTSF*sherpaNJetsWeight*ElecSF*MuonSF*BTagSF*PileupWeight*FinalXSecWeight*GenWeight * 1000)";



#print allCuts["CRZAB_TT_TW"]







